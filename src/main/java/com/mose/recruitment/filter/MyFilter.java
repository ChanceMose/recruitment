package com.mose.recruitment.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @ClassName MyFilter
 * @Description: 过滤器
 * @Author mose
 * @Date 2020/3/15
 * @Version V1.0
 **/
// urlpatterns 过滤哪些请求，filterName 过滤名称
@WebFilter(urlPatterns = "/api/*", filterName = "myFilter")
// 过滤顺序，数字越大越后执行
@Order(1)
public class MyFilter implements Filter {

    @Value("${open.url}")
    private String openUrl;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
