package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Product;
import com.mose.recruitment.model.Welfare;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName WelfareDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/9
 * @Version V1.0
 **/
public interface WelfareDao {

    @Select("select * from welfare where company_id=#{company_id}")
    List<Welfare> getWelfareListByCompany(@Param("company_id") int company_id);

    @Select("select * from welfare where id=#{id}")
    Welfare getWelfare(@Param("id") int id);

    @Insert("insert into welfare (name,detail,company_id) values (#{name},#{detail},#{company_id})")
    int addWelfare(Welfare welfare);

    @Update("update welfare set name=#{name}, detail=#{detail}, company_id=#{company_id} where id=#{id}")
    int updateWelfare(Welfare welfare);

    @Delete("delete from welfare where id=#{id}")
    int deleteWelfare(@Param("id") int id);
}
