package com.mose.recruitment.Dao;

import com.mose.recruitment.model.send.NumberTrend;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @ClassName TrendDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/13
 * @Version V1.0
 **/
public interface TrendDao {

    @Select("SELECT date,count(DATE_FORMAT(date,'%Y%m%d')) as number FROM `apply` where DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date group by DATE_FORMAT(date,'%Y%m%d') order by DATE_FORMAT(date,'%Y%m%d') limit 7")
    List<NumberTrend> getApplyNumberTrendByDay();

    @Select("SELECT name as position,count(name) as number FROM `position` where state=0 group by name order by count(name) desc limit 30")
    List<NumberTrend> getPositionNumberTrend();

    @Select("SELECT city,count(city) as number FROM `position` where state=0 group by city order by count(city) desc limit 10")
    List<NumberTrend> getCityNumberTrend();
}
