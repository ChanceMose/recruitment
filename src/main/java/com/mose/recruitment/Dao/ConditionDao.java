package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Address;
import com.mose.recruitment.model.Condition;
import com.mose.recruitment.model.Position;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName ConditionDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/6
 * @Version V1.0
 **/
public interface ConditionDao {

    @Select("SELECT * FROM conditions WHERE state=#{state}")
    List<Condition> getConditionByState(@Param("state") int state);

    @Select("select id,city from position group by city order by count(city) desc")
    List<Position> getCityListInPosition();

    @Select("select id,city from address group by city order by count(city) desc")
    List<Address> getCityListInCompany();

    @Select("select id,district from position where city=#{city} group by district order by count(district) desc")
    List<Position> getDistrictListByCity(@Param("city") String city);

    @Select("select * from conditions where id=#{id}")
    Condition getCondition(@Param("id") int id);

    @Insert("insert into conditions (name,state) values (#{name},#{state})")
    int addCondition(Condition condition);

    @Update("update conditions set name = #{name}, state=#{state} where id=#{id}")
    int updateCondition(Condition condition);

    @Delete("delete from conditions where id=#{id}")
    int deleteCondition(@Param("id") int id);
}
