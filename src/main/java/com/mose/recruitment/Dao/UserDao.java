package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Position;
import com.mose.recruitment.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName UserDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/12
 * @Version V1.0
 **/
public interface UserDao {

    @Select("select * from user where name like #{query} or telephone like #{query}")
    List<User> getUserListByQuery(@Param("query") String query);

    @Select("select * from user where telephone=#{telephone}")
    User getUserByTelephone(@Param("telephone") String telephone);

    @Select("select * from user where telephone=#{telephone} and password=#{password}")
    User getUserByPhoneAndPassword(@Param("telephone") String telephone, @Param("password") String password);

    @Select("select * from user where id=#{id}")
    User getUser(@Param("id") int id);

    @Insert("insert into user (name,telephone,password,image) values (#{name},#{telephone},#{password},#{image})")
    int addUser(User user);

    @Update("update user set name = #{name}, telephone=#{telephone}, password=#{password}, image=#{image}, state=#{state} where id=#{id}")
    int updateUser(User user);

    @Delete("delete from user where id=#{id}")
    int deleteUser(@Param("id") int id);
}
