package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Apply;
import com.mose.recruitment.model.ApplyList;
import com.mose.recruitment.model.Position;
import com.mose.recruitment.model.UserApplyList;
import org.apache.ibatis.annotations.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName ApplyDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/7
 * @Version V1.0
 **/
public interface ApplyDao {

    @Select("select a.*,b.id as position_id,b.salary_min,b.salary_max,b.city,c.id as company_id,c.shortName from apply a,position b,company c,belongs d where a.resume_id=#{resume_id} and a.state=#{state} " +
            "and a.position=b.name and b.hr_id = a.hr_id and d.id = a.hr_id and c.id = d.company_id group by a.id")
    List<UserApplyList> getApplyListByUserAndState(@Param("resume_id") int resume_id, @Param("state") int state);

    @Select("select * from apply where resume_id=#{resume_id} and hr_id=#{hr_id} and position=#{position}")
    Apply getApplyByUserAndHr(@Param("resume_id") int resume_id, @Param("hr_id") int hr_id, @Param("position") String position);

    @Select("select a.id,a.resume_id,a.hr_id,a.position,a.date,a.state,c.realName,b.image,c.sex,c.education,c.years,c.city,c.email,d.telephone " +
            "from (select * from apply) a,(select id,image from resume) b,(select * from baseinfo) c,(select id,telephone from user) d " +
            "where a.resume_id = b.id and b.id=c.id and c.id=d.id and a.hr_id=#{hr_id} and (a.state=#{state1} or a.state=#{state2}) order by a.date")
    List<ApplyList> getApplyListByHrAndTwoState(@Param("hr_id") int hr_id, @Param("state1") int state1, @Param("state2") int state2);

    @Select("select a.id,a.resume_id,a.hr_id,a.position,a.date,a.state,c.realName,b.image,c.sex,c.education,c.years,c.city,c.email,d.telephone " +
            "from (select * from apply) a,(select id,image from resume) b,(select * from baseinfo) c,(select id,telephone from user) d " +
            "where a.resume_id = b.id and b.id=c.id and c.id=d.id and a.hr_id=#{hr_id} and a.state=#{state} order by a.date")
    List<ApplyList> getApplyListByHr(@Param("hr_id") int hr_id, @Param("state") int state);

    @Select("select * from apply where id=#{id}")
    Apply getApply(@Param("id") int id);

    @Insert("insert into apply (resume_id,hr_id,position,date,state) values (#{resume_id},#{hr_id},#{position},#{date},#{state})")
    int addApply(Apply apply);

    @Update("update apply set resume_id = #{resume_id}, hr_id=#{hr_id}, position=#{position}, date=#{date}, state=#{state} where id=#{id}")
    int updateApply(Apply apply);

    @Delete("delete from apply where id=#{id}")
    int deleteApply(@Param("id") int id);
}
