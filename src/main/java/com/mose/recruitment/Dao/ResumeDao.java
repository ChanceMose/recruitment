package com.mose.recruitment.Dao;

import com.mose.recruitment.model.resume.*;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName ResumeDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/1
 * @Version V1.0
 **/
public interface ResumeDao {

    @Select("SELECT a.id, b.realName, a.image, b.sex, b.birth, b.education, d.city, b.years, d.wanted " +
            "FROM ( SELECT id, image, state, visible FROM resume ) a, ( SELECT id, realName, sex, birth, education, years FROM baseinfo ) b, " +
            "( SELECT id, city, company_id FROM address ) c, ( SELECT id, wanted, city, user_id FROM intention ) d, (select * from user where state=0) e " +
            "WHERE " +
            "a.id = b.id AND b.id = d.user_id AND b.id = e.id  AND a.state <> \"暂时不换工作\" AND a.visible = 0 " +
            "AND c.company_id = #{company_id} AND c.city = d.city")
    List<ResumeList> getResumeListByFilter(@Param("company_id") int company_id);

    @Select("SELECT a.id, b.realName, a.image, b.sex, b.birth, b.education, d.city, b.years, d.wanted " +
            "FROM ( SELECT id, image, state, visible FROM resume ) a, ( SELECT id, realName, sex, birth, education, years FROM baseinfo ) b, " +
            "( SELECT id, city, company_id FROM address ) c, ( SELECT id, wanted, city, user_id FROM intention ) d, (select * from user where state=0) e " +
            "WHERE " +
            "a.id = b.id AND b.id = d.user_id AND b.id = e.id  AND a.state <> \"暂时不换工作\" AND a.visible = 0 " +
            "AND c.company_id = #{company_id} AND c.city = d.city AND d.wanted=#{position}")
    List<ResumeList> getResumeListByPosition(@Param("position") String position, @Param("company_id") int company_id);

    // 基础信息
    @Select("select * from baseinfo where id=#{id}")
    BaseInfo getBaseInfo(@Param("id") int id);

    @Insert("insert into baseinfo (id,realName,birth,sex,city,email,education,years) values (#{id},#{realName},#{birth},#{sex},#{city},#{email},#{education},#{years})")
    int addBaseInfo(BaseInfo baseInfo);

    @Update("update baseinfo set realName = #{realName}, birth = #{birth}, sex=#{sex}, city=#{city}, email=#{email}, education=#{education},years=#{years} where id=#{id}")
    int updateBaseInfo(BaseInfo baseInfo);

    @Delete("delete from baseinfo where id=#{id}")
    int deleteBaseInfo(@Param("id") int id);

    // 简历信息
    @Select("select * from resume where id=#{id}")
    Resume getResume(@Param("id") int id);

    @Insert("insert into resume (id,create_time,image,address,state,apply,description) values (#{id},#{create_time},#{image},#{address},#{state},#{apply},#{description})")
    int addResume(Resume resume);

    @Update("update resume set create_time = #{create_time}, image=#{image}, address=#{address}, state=#{state}, apply=#{apply}, description=#{description}, visible=#{visible} where id=#{id}")
    int updateResume(Resume resume);

    @Delete("delete from resume where id=#{id}")
    int deleteResume(@Param("id") int id);

    // 教育经历
    @Select("select * from education where user_id=#{user_id} order by state desc")
    List<Education> getEducationList(@Param("user_id") int user_id);

    @Insert("insert into education (school,entrance,graduation,record,speciality,state,user_id) values (#{school},#{entrance},#{graduation},#{record},#{speciality},#{state},#{user_id})")
    int addEducation(Education education);

    @Update("update education set school = #{school}, entrance=#{entrance}, graduation=#{graduation}, record=#{record}, speciality=#{speciality}, state=#{state} where id=#{id}")
    int updateEducation(Education education);

    @Delete("delete from education where id=#{id}")
    int deleteEducation(@Param("id") int id);

    // 工作经历
    @Select("select * from work where user_id=#{user_id}")
    List<Work> getWorkList(@Param("user_id") int user_id);

    @Insert("insert into work (company,label,department,type,name,entry,quit,description,user_id) values (#{company},#{label},#{department},#{type},#{name},#{entry},#{quit},#{description},#{user_id})")
    int addWork(Work work);

    @Update("update work set company = #{company}, label=#{label}, department=#{department}, type=#{type}, name=#{name}, entry=#{entry}, quit=#{quit}, description=#{description}, user_id=#{user_id} where id=#{id}")
    int updateWork(Work education);

    @Delete("delete from work where id=#{id}")
    int deleteWork(@Param("id") int id);

    // 项目经历
    @Select("select * from project where user_id=#{user_id}")
    List<Project> getProjectList(@Param("user_id") int user_id);

    @Insert("insert into project (name,company,start,end,url,description,achievement,user_id) values (#{name},#{company},#{start},#{end},#{url},#{description},#{achievement},#{user_id})")
    int addProject(Project project);

    @Update("update project set name = #{name}, company=#{company}, start=#{start}, end=#{end}, url=#{url}, description=#{description}, achievement=#{achievement}, user_id=#{user_id} where id=#{id}")
    int updateProject(Project project);

    @Delete("delete from project where id=#{id}")
    int deleteProject(@Param("id") int id);

    // 求职意向
    @Select("select * from intention where user_id=#{user_id}")
    List<Intention> getIntentionList(@Param("user_id") int user_id);

    @Insert("insert into intention (wanted,city,salary_min,salary_max,user_id) values (#{wanted},#{city},#{salary_min},#{salary_max},#{user_id})")
    int addIntention(Intention intention);

    @Update("update intention set wanted = #{wanted}, city=#{city}, salary_min=#{salary_min}, salary_max=#{salary_max}, user_id=#{user_id} where id=#{id}")
    int updateIntention(Intention intention);

    @Delete("delete from intention where id=#{id}")
    int deleteIntention(@Param("id") int id);
}
