package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Admin;
import org.apache.ibatis.annotations.*;

/**
 * @ClassName AdminDao
 * @Description: 获取数据库中admin表数据的接口
 * @Author mose
 * @Date 2020/1/21
 * @Version V1.0
 **/
public interface AdminDao {

    @Select("select * from admin where name=#{name} and password=#{password}")
    Admin getAdminByNameAndPassword(Admin admin);

    @Select("select * from admin where id=#{id}")
    Admin getAdmin(@Param("id") int id);

    @Insert("insert into admin (name,password) values (#{name},#{password})")
    int addAdmin(Admin admin);

    @Update("update admin set name = #{name}, password=#{password}")
    int updateAdmin(Admin admin);

    @Delete("delete from admin where id=#{id}")
    int deleteAdmin(@Param("id") int id);
}