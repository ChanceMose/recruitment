package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Belong;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName BelongDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/28
 * @Version V1.0
 **/
public interface BelongDao {

    @Select("select a.* from belongs a, position b where b.id=#{id} and b.hr_id=a.id")
    Belong getBelongByPosition(@Param("id") int id);

    @Select("select * from belongs where company_id=#{company_id}")
    List<Belong> getBelongsByCompanyId(@Param("company_id") int company_id);

    @Select("select * from belongs where id=#{id}")
    Belong getBelong(@Param("id") int id);

    @Insert("insert into belongs (id,company_id,realName,position,image,email) values (#{id},#{company_id},#{realName},#{position},#{image},#{email})")
    int addBelong(Belong belong);

    @Update("update belongs set company_id = #{company_id}, realName=#{realName}, position=#{position}, image=#{image}, email=#{email} where id=#{id}")
    int updateBelong(Belong belong);

    @Delete("delete from belongs where id=#{id}")
    int deleteBelong(@Param("id") int id);
}
