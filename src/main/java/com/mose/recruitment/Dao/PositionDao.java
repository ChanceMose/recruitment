package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Position;
import com.mose.recruitment.model.send.PositionList;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName PositionDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/2/28
 * @Version V1.0
 **/
public interface PositionDao {

    @Select("select a.* from position a,belongs b,company c where c.id=#{company_id} and a.state=#{state} and a.hr_id=b.id and b.company_id=c.id")
    List<Position> getPositionListByCompany(@Param("company_id") int company_id, @Param("state") int state);

    @Select("SELECT a.*, b.id AS company_id, b.shortName " +
            "FROM position a, ( SELECT id, shortName FROM company ) b, ( SELECT * FROM belongs ) c " +
            "WHERE a.hr_id = c.id AND b.id = c.company_id AND (a.name like #{query} OR b.shortName like #{query})")
    List<PositionList> getPositionList(@Param("query") String query);

    @Select("select * from position where hr_id=#{hr_id} and state=#{state} order by create_time desc")
    List<Position> getPositionListByState(@Param("hr_id") int hr_id, @Param("state") int state);

    @Select("select a.id,a.name from position a,company b,belongs c where b.id=#{company_id} and b.id=c.company_id and a.hr_id=c.id and a.state=0 group by a.name")
    List<Position> getPositionNameListByCompany(@Param("company_id") int company_id);

    @Select("SELECT a.*,b.id as company_id,b.shortName, b.type, b.financingType, b.population, b.image,d.city FROM " +
            "position a, ( SELECT id, shortName, type, financingType, population, image FROM company ) b, (SELECT * FROM belongs) c,(SELECT city,company_id FROM address GROUP BY company_id) d " +
            "WHERE a.hr_id = c.id AND " +
            "c.company_id = b.id AND " +
            "d.company_id = b.id AND " +
            "a.state = 0 ORDER BY a.create_time DESC LIMIT #{number}")
    List<PositionList> getPositionListByLimitNumber(@Param("number") int number);

    @Select("SELECT a.id, a.name, a.create_time, a.salary_min, a.salary_max, a.years, a.education, a.advantage, a.city, " +
            "b.id as company_id,b.shortName, b.type, b.financingType, b.population, b.image FROM " +
            "position a, ( SELECT id, shortName, type, financingType, population, image FROM company ) b, ( SELECT * FROM belongs ) c " +
            "WHERE a.hr_id = c.id  AND c.company_id = b.id " +
            "AND a.name LIKE IF (#{search} ='%%',a.name,#{search}) " +
            "AND a.city = IF (#{city} = '',a.city,#{city}) " +
            "AND a.district = IF (#{district} = '',a.district,#{district}) " +
            "AND a.years = IF (#{experience} = '',a.years,#{experience}) " +
            "AND a.salary_min >= IF (#{salary_min} =  -1,a.salary_min,#{salary_min}) " +
            "AND a.salary_max <= IF (#{salary_max} =  -1,a.salary_max,#{salary_max}) " +
            "AND a.education = IF (#{education} =  '',a.education,#{education}) " +
            "AND b.financingType = IF (#{finance} = '',b.financingType,#{finance}) " +
            "AND b.population = IF (#{companySize} = '',b.population,#{companySize}) " +
            "AND b.type LIKE IF (#{field} = '',b.type,#{field}) " +
            "AND a.state = 0 " +
            "ORDER BY a.create_time DESC LIMIT 300")
    List<PositionList> getPositionListByFilter(@Param("search") String search, @Param("city") String city, @Param("district") String district, @Param("experience") String experience, @Param("education") String education, @Param("salary_min") int salary_min, @Param("salary_max") int salary_max, @Param("finance") String finance, @Param("companySize") String companySize, @Param("field") String field);

    @Select("select * from position where id=#{id}")
    Position getPosition(@Param("id") int id);

    @Insert("insert into position (name,create_time,salary_min,salary_max,city,district,address,years,education,advantage,description,hr_id)" +
            " values (#{name},#{create_time},#{salary_min},#{salary_max},#{city},#{district},#{address},#{years},#{education},#{advantage},#{description},#{hr_id})")
    int addPosition(Position position);

    @Update("update position set name = #{name}, create_time=#{create_time}, salary_min=#{salary_min}, salary_max=#{salary_max}, city=#{city}, district=#{district}, address=#{address}, years=#{years}, " +
            "education=#{education}, advantage=#{advantage}, description=#{description}, hr_id=#{hr_id}, state=#{state}, legal=#{legal} where id=#{id}")
    int updatePosition(Position position);

    @Delete("delete from position where id=#{id}")
    int deletePosition(@Param("id") int id);

}
