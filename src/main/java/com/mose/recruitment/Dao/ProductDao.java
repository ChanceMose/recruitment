package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName ProductDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/9
 * @Version V1.0
 **/
public interface ProductDao {

    @Select("select * from product where company_id=#{company_id}")
    List<Product> getProductListByCompany(@Param("company_id") int company_id);

    @Select("select * from product where id=#{id}")
    Product getProduct(@Param("id") int id);

    @Insert("insert into product (name,introduction,image,url,company_id) values (#{name},#{introduction},#{image},#{url},#{company_id})")
    int addProduct(Product product);

    @Update("update product set name = #{name}, introduction=#{introduction}, image=#{image}, url=#{url}, company_id=#{company_id} where id=#{id}")
    int updateProduct(Product product);

    @Delete("delete from product where id=#{id}")
    int deleteProduct(@Param("id") int id);
}
