package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Address;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName AddressDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/9
 * @Version V1.0
 **/
public interface AddressDao {

    @Select("select * from address where company_id=#{company_id}")
    List<Address> getAddressListByCompany(@Param("company_id") int company_id);

    @Select("select * from address where id=#{id}")
    Address getAddress(@Param("id") int id);

    @Insert("insert into address (city,district,address,company_id) values (#{city},#{district},#{address},#{company_id})")
    int addAddress(Address address);

    @Update("update address set city=#{city}, district=#{district}, address=#{address}, company_id=#{company_id} where id=#{id}")
    int updateAddress(Address address);

    @Delete("delete from address where id=#{id}")
    int deleteAddress(@Param("id") int id);
}
