package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Job;
import com.mose.recruitment.model.JobTree;
import com.mose.recruitment.model.Position;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName JobDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/2/23
 * @Version V1.0
 **/
public interface JobDao {

    @Select("select * from job where level=#{level}")
    List<Job> getJobListByLevel(@Param("level") int level);

    @Select("select * from job where name like #{query}")
    List<Job> getJobList(@Param("query") String query);

    @Select("select * from job")
    List<JobTree> getJobByAll();

    @Select("select * from job where id=#{id}")
    Job getJob(@Param("id") int id);

    @Insert("insert into job (name,parentId,level) values (#{name},#{parentId},#{level})")
    int addJob(Job job);

    @Update("update job set name = #{name}, parentId=#{parentId}, level=#{level} where id=#{id}")
    int updateJob(Job job);

    @Delete("delete from job where id=#{id}")
    int deleteJob(@Param("id") int id);
}
