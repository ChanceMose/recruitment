package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Job;
import com.mose.recruitment.model.Type;
import com.mose.recruitment.model.TypeTree;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName TypeDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/14
 * @Version V1.0
 **/
public interface TypeDao {

    @Select("select * from type where level=#{level}")
    List<Type> getTypeListByLevel(@Param("level") int level);

    @Select("select * from type")
    List<TypeTree> getTypeAll();

    @Select("select * from type where name like #{query}")
    List<Type> getTypeList(@Param("query") String query);

    @Select("select * from type where id=#{id}")
    Type getType(@Param("id") int id);

    @Insert("insert into type (name,parentId,level) values (#{name},#{parentId},#{level})")
    int addType(Type type);

    @Update("update type set name = #{name}, parentId=#{parentId}, level=#{level} where id=#{id}")
    int updateType(Type type);

    @Delete("delete from type where id=#{id}")
    int deleteType(@Param("id") int id);
}
