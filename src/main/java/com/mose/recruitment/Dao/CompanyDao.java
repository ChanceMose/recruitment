package com.mose.recruitment.Dao;

import com.mose.recruitment.model.Company;
import com.mose.recruitment.model.Position;
import com.mose.recruitment.model.send.CompanyList;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @ClassName CompanyDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/10
 * @Version V1.0
 **/
public interface CompanyDao {

    @Select("select a.* from company a, belongs b,position c where c.id=#{id} and c.hr_id=b.id and b.company_id=a.id")
    Company getCompanyByPosition(@Param("id") int id);

    @Select("select id,fullName from company")
    List<Company> getCompanyNameList();

    @Select("select a.*,b.city from company a,(select company_id,city from address group by company_id) b where a.id=b.company_id limit 720")
    List<CompanyList> getCompanyList();

    @Select("select a.*,b.city from company a,(select company_id,city from address group by company_id) b where a.id=b.company_id and a.shortName like #{search} limit 300")
    List<CompanyList> getCompanyListBySearch(@Param("search") String search);

    @Select("select a.id,a.shortName,a.image,a.type,a.financingType,a.population,a.word,b.city from " +
            "company a,(select company_id,city from address group by company_id) b where " +
            "a.id=b.company_id and b.city = if (#{city} = \"\",b.city,#{city}) and " +
            "a.financingType = if (#{finance} = \"\",a.financingType,#{finance}) and " +
            "a.population = if (#{companySize} = \"\",a.population,#{companySize}) and " +
            "a.type like if (#{field} = \"\",a.type,#{field}) limit 720")
    List<CompanyList> getCompanyListByFilter(@Param("city") String city, @Param("finance") String finance, @Param("companySize") String companySize, @Param("field") String field);

    @Select("select * from company where id=#{id}")
    Company getCompany(@Param("id") int id);

    @Insert("insert into company (shortName,fullName,type,image,word,financingType,population,homepage,labels,introduction,createTime,capital,representative)" +
            " values (#{shortName},#{fullName},#{type},#{image},#{word},#{financingType},#{population},#{homepage},#{labels},#{introduction},#{createTime},#{capital},#{representative})")
    int addCompany(Company company);

    @Update("update company set shortName = #{shortName}, fullName=#{fullName}, type=#{type}, image=#{image}, word=#{word}, financingType=#{financingType}, population=#{population}, homepage=#{homepage}, " +
            "labels=#{labels}, introduction=#{introduction}, createTime=#{createTime}, capital=#{capital}, representative=#{representative}, legal=#{legal} where id=#{id}")
    int updateCompany(Company company);

    @Delete("delete from company where id=#{id}")
    int deleteCompany(@Param("id") int id);
}
