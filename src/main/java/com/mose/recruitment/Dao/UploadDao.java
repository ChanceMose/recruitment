package com.mose.recruitment.Dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @ClassName UploadDao
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/31
 * @Version V1.0
 **/
public interface UploadDao {

    @Select("select image from ${table} where id=#{id}")
    String getImage(@Param("table") String table, @Param("id") int id);

    @Update("update ${table} set image=#{image} where id=#{id}")
    int updateImage(@Param("table") String table, @Param("image") String image, @Param("id") int id);

    @Select("select file from resume where id=#{id}")
    String getFile(@Param("id") int id);

    @Update("update resume set file=#{file} where id=#{id}")
    int updateFile(@Param("file") String file, @Param("id") int id);
}
