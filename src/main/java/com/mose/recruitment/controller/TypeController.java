package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.PassToken;
import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Job;
import com.mose.recruitment.model.Type;
import com.mose.recruitment.model.TypeTree;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.TypeService;
import com.mose.recruitment.utils.ResultUtil;
import com.mose.recruitment.utils.TreeParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName TypeController
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/14
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class TypeController {

    @Autowired
    TypeService typeService;

    @PassToken
    @GetMapping("/types/tree")
    public MyResult<List<TypeTree>> getTypeTreeList() {
        List<TypeTree> typeList = typeService.getTypeAll();
        List<TypeTree> result = TreeParser.getTreeList(0, typeList);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    @PassToken
    @GetMapping("/types/{level}")
    public MyResult<List<Type>> getTypeListByLevel(@PathVariable Integer level) {
        List<Type> result = typeService.getTypeListByLevel(level);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    @PassToken
    @GetMapping("/types")
    public MyResult<List<Type>> getTypeList(@RequestParam String query) {
        query = "%" + query + "%";
        List<Type> result = typeService.getTypeList(query);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: type
     * @Description: 职位类别的crud
     * @Author: mose
     * @Date: 2020/4/14 22:56
     */
    @PassToken
    @GetMapping("/type/{id}")
    public MyResult<Type> getType(@PathVariable Integer id) {
        Type result = typeService.getType(id);

        if (result == null) {
            return ResultUtil.noExit("获取职位类别详情失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/type")
    public MyResult<Void> addType(@RequestBody Type type) {
        if (typeService.addType(type) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/type")
    public MyResult<Void> updateType(@RequestBody Type type) {
        if (typeService.updateType(type) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/type/{id}")
    public MyResult<Void> deleteType(@PathVariable Integer id) {
        if (typeService.deleteType(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
