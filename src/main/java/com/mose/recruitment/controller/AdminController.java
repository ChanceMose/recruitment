package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.PassToken;
import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Admin;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.AdminService;
import com.mose.recruitment.utils.Base64Util;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName AdminController
 * @Description: 对 管理员 用户的操作
 * @Author mose
 * @Date 2020/1/21
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class AdminController {

    @Autowired
    AdminService adminService;

    /***
     *
     * @MethodName: getAdminByNameAndPassword
     * @Description: 管理员登陆
     * @param admin
     * @Return: com.mose.recruitment.model.send.MyResult<com.mose.recruitment.model.Admin>
     * @Author: mose
     * @Date: 2020/4/11 20:35
     */
    @PassToken
    @PostMapping("/admin/login")
    public MyResult<Admin> getAdminByNameAndPassword(@RequestBody Admin admin) {
        admin.setPassword(Base64Util.encode(admin.getPassword()));
        Admin userResult = adminService.getAdminByNameAndPassword(admin);

        if (userResult == null) {
            return ResultUtil.noExit("账号或者密码错误");
        }
        Map result = new HashMap();
        result.put("token", "admin");
        result.put("user", userResult);
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: admin
     * @Description: Admin的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @UserLoginToken
    @GetMapping("/admin/{id}")
    public MyResult<Admin> getAdmin(@PathVariable Integer id) {
        Admin result = adminService.getAdmin(id);

        if (result == null) {
            return ResultUtil.noExit("获取管理员失败");
        }
        result.setPassword(Base64Util.decode(result.getPassword()));
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/admin")
    public MyResult<Void> addAdmin(@RequestBody Admin admin) {
        admin.setPassword(Base64Util.encode(admin.getPassword()));
        if (adminService.addAdmin(admin) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/admin")
    public MyResult<Void> updateAdmin(@RequestBody Admin admin) {
        admin.setPassword(Base64Util.encode(admin.getPassword()));
        if (adminService.updateAdmin(admin) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/admin/{id}")
    public MyResult<Void> deleteAdmin(@PathVariable Integer id) {
        if (adminService.deleteAdmin(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
