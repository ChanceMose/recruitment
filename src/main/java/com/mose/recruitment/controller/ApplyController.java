package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Apply;
import com.mose.recruitment.model.ApplyList;
import com.mose.recruitment.model.UserApplyList;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.ApplyService;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName ApplyController
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/7
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class ApplyController {

    @Autowired
    ApplyService applyService;

    /***
     *
     * @MethodName: getApplyListByUserAndState
     * @Description: 根据求职者获取简历投递列表
     * @param resume_id
     * @param state
     * @Return: com.mose.recruitment.model.UserApplyList<java.util.List                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               com.mose.recruitment.model.ApplyList>>
     * @Author: mose
     * @Date: 2020/4/7 21:13
     */
    @UserLoginToken
    @GetMapping("/applies/user/{resume_id}/{state}")
    public MyResult<List<UserApplyList>> getApplyListByUserAndState(@PathVariable Integer resume_id, @PathVariable Integer state) {
        List<UserApplyList> result = applyService.getApplyListByUserAndState(resume_id, state);

        if (result == null) {
            return ResultUtil.noExit("获取简历投递列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getApplyByUserAndHr
     * @Description: 根据求职者和Hr获取简历投递
     * @param resume_id
     * @param hr_id
     * @Return: com.mose.recruitment.model.Apply                                                          <                                                               com.mose.recruitment.model.ApplyList>>
     * @Author: mose
     * @Date: 2020/4/7 21:13
     */
    @UserLoginToken
    @GetMapping("/apply/user/{resume_id}/{hr_id}/{position}")
    public MyResult<Apply> getApplyByUserAndHr(@PathVariable Integer resume_id, @PathVariable Integer hr_id, @PathVariable String position) {
        Apply result = applyService.getApplyByUserAndHr(resume_id, hr_id, position);

        if (result == null) {
            return ResultUtil.noExit("没有投递过");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getApplyListByHr
     * @Description: 根据Hr获取简历投递列表未读和已读
     * @param hr_id
     * @param state1
     * @param state2
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               com.mose.recruitment.model.ApplyList>>
     * @Author: mose
     * @Date: 2020/4/7 21:13
     */
    @UserLoginToken
    @GetMapping("/applies/{hr_id}/{state1}/{state2}")
    public MyResult<List<ApplyList>> getApplyListByHrAndTwoState(@PathVariable Integer hr_id, @PathVariable Integer state1, @PathVariable Integer state2) {
        List<ApplyList> result = applyService.getApplyListByHrAndTwoState(hr_id, state1, state2);

        if (result == null) {
            return ResultUtil.noExit("获取简历投递列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getApplyListByHr
     * @Description: 根据Hr获取简历投递列表
     * @param hr_id
     * @param state
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               com.mose.recruitment.model.ApplyList>>
     * @Author: mose
     * @Date: 2020/4/7 21:13
     */
    @UserLoginToken
    @GetMapping("/applies/{hr_id}/{state}")
    public MyResult<List<ApplyList>> getApplyListByHr(@PathVariable Integer hr_id, @PathVariable Integer state) {
        List<ApplyList> result = applyService.getApplyListByHr(hr_id, state);

        if (result == null) {
            return ResultUtil.noExit("获取简历投递列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: position
     * @Description: 投递简历的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @UserLoginToken
    @GetMapping("/apply/{id}")
    public MyResult<Apply> getApply(@PathVariable Integer id) {
        Apply result = applyService.getApply(id);

        if (result == null) {
            return ResultUtil.noExit("获取简历投递情况失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/apply")
    public MyResult<Void> addApply(@RequestBody Apply apply) {
        if (applyService.addApply(apply) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/apply")
    public MyResult<Void> updateApply(@RequestBody Apply apply) {
        if (applyService.updateApply(apply) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/apply/{id}")
    public MyResult<Void> deleteApply(@PathVariable Integer id) {
        if (applyService.deleteApply(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
