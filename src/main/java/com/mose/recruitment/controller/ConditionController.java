package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.PassToken;
import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Address;
import com.mose.recruitment.model.Condition;
import com.mose.recruitment.model.Position;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.ConditionService;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName SearchFilterController
 * @Description: 搜索过滤条件控制类
 * @Author mose
 * @Date 2020/3/6
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class ConditionController {

    @Autowired
    ConditionService conditionService;

    /***
     *
     * @MethodName: getConditionByState
     * @Description: 根据state获取其条件列表
     * @param state
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List<com.mose.recruitment.model.Condition>>
     * @Author: mose
     * @Date: 2020/4/3 18:33
     */
    @PassToken
    @GetMapping("/conditions/{state}")
    public MyResult<List<Condition>> getConditionByState(@PathVariable Integer state) {
        List<Condition> result = conditionService.getConditionByState(state);

        if (result == null) {
            return ResultUtil.noExit("获取过滤条件失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getCityListInPosition
     * @Description: 获取职位所属城市列表
     * @param 
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List<com.mose.recruitment.model.Position>>
     * @Author: mose
     * @Date: 2020/4/3 18:34
     */
    @PassToken
    @GetMapping("/positions/cities")
    public MyResult<List<Position>> getCityListInPosition() {
        List<Position> result = conditionService.getCityListInPosition();

        while (result.remove(null)) ;

        if (result == null) {
            return ResultUtil.noExit("获取城市失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getCityListInCompany
     * @Description: 获取公司所属城市列表
     * @param 
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List<com.mose.recruitment.model.Address>>
     * @Author: mose
     * @Date: 2020/4/3 18:34
     */
    @PassToken
    @GetMapping("/companies/cities")
    public MyResult<List<Address>> getCityListInCompany() {
        List<Address> result = conditionService.getCityListInCompany();

        while (result.remove(null)) ;

        if (result == null) {
            return ResultUtil.noExit("获取城市失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getDistrictListByCity
     * @Description: 获取城市的行政区
     * @param city
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List<com.mose.recruitment.model.Position>>
     * @Author: mose
     * @Date: 2020/4/3 18:34
     */
    @GetMapping("/district/{city}")
    public MyResult<List<Position>> getDistrictListByCity(@PathVariable String city) {
        List<Position> result = conditionService.getDistrictListByCity(city);

        while (result.remove(null)) ;

        if (result == null) {
            return ResultUtil.noExit("获取行政区失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: condition
     * @Description: 条件的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @UserLoginToken
    @GetMapping("/condition/{id}")
    public MyResult<Condition> getCondition(@PathVariable Integer id) {
        Condition result = conditionService.getCondition(id);

        if (result == null) {
            return ResultUtil.noExit("获取条件详情失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/condition")
    public MyResult<Void> addCondition(@RequestBody Condition condition) {
        if (conditionService.addCondition(condition) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/condition")
    public MyResult<Void> updateCondition(@RequestBody Condition condition) {
        if (conditionService.updateCondition(condition) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/condition/{id}")
    public MyResult<Void> deleteCondition(@PathVariable Integer id) {
        if (conditionService.deleteCondition(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }

}
