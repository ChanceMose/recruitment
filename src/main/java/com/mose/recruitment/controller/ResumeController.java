package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.resume.*;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.ResumeService;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName ResumeController
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/1
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class ResumeController {

    @Autowired
    ResumeService resumeService;

    /***
     *
     * @MethodName: getResumeListByPosition
     * @Description: 根据公司id获取简历信息
     * @param company_id
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List < com.mose.recruitment.model.resume.ResumeList>>
     * @Author: mose
     * @Date: 2020/4/5 19:08
     */
    @UserLoginToken
    @GetMapping("/resumes/{company_id}")
    public MyResult<List<ResumeList>> getResumeListByFilter(@PathVariable Integer company_id) {
        List<ResumeList> result = resumeService.getResumeListByFilter(company_id);

        if (result == null) {
            return ResultUtil.noExit("获取简历列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getResumeListByPosition
     * @Description: 根据职位名称和公司id获取简历信息
     * @param position
     * @param company_id
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List < com.mose.recruitment.model.resume.ResumeList>>
     * @Author: mose
     * @Date: 2020/4/5 19:08
     */
    @UserLoginToken
    @GetMapping("/resumes/{position}/{company_id}")
    public MyResult<List<ResumeList>> getResumeListByPosition(@PathVariable String position, @PathVariable Integer company_id) {
        List<ResumeList> result = resumeService.getResumeListByPosition(position, company_id);

        if (result == null) {
            return ResultUtil.noExit("获取简历列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: baseinfo
     * @Description: 基础信息的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @UserLoginToken
    @GetMapping("/baseinfo/{id}")
    public MyResult<BaseInfo> getBaseInfo(@PathVariable Integer id) {
        BaseInfo result = resumeService.getBaseInfo(id);

        if (result == null) {
            return ResultUtil.noExit("获取基础信息失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/baseinfo")
    public MyResult<Void> addBaseInfo(@RequestBody BaseInfo baseInfo) {
        if (resumeService.addBaseInfo(baseInfo) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/baseinfo")
    public MyResult<Void> updateBaseInfo(@RequestBody BaseInfo baseInfo) {
        if (resumeService.updateBaseInfo(baseInfo) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/baseinfo/{id}")
    public MyResult<Void> deleteBaseInfo(@PathVariable Integer id) {
        if (resumeService.deleteBaseInfo(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }


    /***
     *
     * @Name: resume
     * @Description: 简历信息的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @UserLoginToken
    @GetMapping("/resume/{id}")
    public MyResult<Resume> getResume(@PathVariable Integer id) {
        Resume result = resumeService.getResume(id);

        if (result == null) {
            return ResultUtil.noExit("获取简历信息失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/resume")
    public MyResult<Void> addResume(@RequestBody Resume resume) {
        if (resumeService.addResume(resume) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/resume")
    public MyResult<Void> updateResume(@RequestBody Resume resume) {
        if (resumeService.updateResume(resume) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/resume/{id}")
    public MyResult<Void> deleteResume(@PathVariable Integer id) {
        if (resumeService.deleteResume(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }

    /***
     *
     * @Name: education
     * @Description: 教育经历的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @UserLoginToken
    @GetMapping("/education/{user_id}")
    public MyResult<List<Education>> getEducationList(@PathVariable Integer user_id) {
        List<Education> result = resumeService.getEducationList(user_id);

        if (result == null) {
            return ResultUtil.noExit("获取教育经历失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/education")
    public MyResult<Void> addEducation(@RequestBody Education education) {
        if (resumeService.addEducation(education) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/education")
    public MyResult<Void> updateEducation(@RequestBody Education education) {
        if (resumeService.updateEducation(education) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/education/{id}")
    public MyResult<Void> deleteEducation(@PathVariable Integer id) {
        if (resumeService.deleteEducation(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }

    /***
     *
     * @Name: work
     * @Description: 工作经历的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @UserLoginToken
    @GetMapping("/work/{user_id}")
    public MyResult<List<Work>> getWorkList(@PathVariable Integer user_id) {
        List<Work> result = resumeService.getWorkList(user_id);

        if (result == null) {
            return ResultUtil.noExit("获取工作经历失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/work")
    public MyResult<Void> addWork(@RequestBody Work work) {
        if (resumeService.addWork(work) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/work")
    public MyResult<Void> updateWork(@RequestBody Work work) {
        if (resumeService.updateWork(work) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/work/{id}")
    public MyResult<Void> deleteWork(@PathVariable Integer id) {
        if (resumeService.deleteWork(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }

    /***
     *
     * @Name: project
     * @Description: 项目经历的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @UserLoginToken
    @GetMapping("/project/{user_id}")
    public MyResult<List<Project>> getProjectList(@PathVariable Integer user_id) {
        List<Project> result = resumeService.getProjectList(user_id);

        if (result == null) {
            return ResultUtil.noExit("获取项目经历失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/project")
    public MyResult<Void> addProject(@RequestBody Project project) {
        if (resumeService.addProject(project) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/project")
    public MyResult<Void> updateProject(@RequestBody Project project) {
        if (resumeService.updateProject(project) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/project/{id}")
    public MyResult<Void> deleteProject(@PathVariable Integer id) {
        if (resumeService.deleteProject(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }

    /***
     *
     * @Name: intention
     * @Description: 求职意向的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @UserLoginToken
    @GetMapping("/intention/{user_id}")
    public MyResult<List<Intention>> getIntentionList(@PathVariable Integer user_id) {
        List<Intention> result = resumeService.getIntentionList(user_id);

        if (result == null) {
            return ResultUtil.noExit("获取求职意向失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/intention")
    public MyResult<Void> addIntention(@RequestBody Intention intention) {
        if (resumeService.addIntention(intention) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/intention")
    public MyResult<Void> updateIntention(@RequestBody Intention intention) {
        if (resumeService.updateIntention(intention) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/intention/{id}")
    public MyResult<Void> deleteIntention(@PathVariable Integer id) {
        if (resumeService.deleteIntention(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
