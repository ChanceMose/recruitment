package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.PassToken;
import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Welfare;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.WelfareService;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName WelfareController
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/9
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class WelfareController {

    @Autowired
    WelfareService welfareService;

    /***
     *
     * @MethodName: getWelfareListByCompany
     * @Description: 根据公司获取其所有福利
     * @param company_id
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List<com.mose.recruitment.model.Welfare>>
     * @Author: mose
     * @Date: 2020/4/9 20:55
     */
    @PassToken
    @GetMapping("/welfares/{company_id}")
    public MyResult<List<Welfare>> getWelfareListByCompany(@PathVariable Integer company_id) {
        List<Welfare> result = welfareService.getWelfareListByCompany(company_id);

        if (result == null) {
            return ResultUtil.noExit("获取产品列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: welfare
     * @Description: 公司福利的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @PassToken
    @GetMapping("/welfare/{id}")
    public MyResult<Welfare> getWelfare(@PathVariable Integer id) {
        Welfare result = welfareService.getWelfare(id);

        if (result == null) {
            return ResultUtil.noExit("获取产品失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/welfare")
    public MyResult<Void> addWelfare(@RequestBody Welfare welfare) {
        if (welfareService.addWelfare(welfare) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/welfare")
    public MyResult<Void> updateWelfare(@RequestBody Welfare welfare) {
        if (welfareService.updateWelfare(welfare) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/welfare/{id}")
    public MyResult<Void> deleteWelfare(@PathVariable Integer id) {
        if (welfareService.deleteWelfare(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
