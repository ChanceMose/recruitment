package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.PassToken;
import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Position;
import com.mose.recruitment.model.User;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.TokenService;
import com.mose.recruitment.service.UserService;
import com.mose.recruitment.utils.Base64Util;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @ClassName UserController
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/12
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    TokenService tokenService;

    @UserLoginToken
    @GetMapping("/users")
    public MyResult<List<User>> getUserListByQuery(@RequestParam String query) {
        query = "%" + query + "%";
        List<User> result = userService.getUserListByQuery(query);
        if (result == null) {
            return ResultUtil.noExit("用户列表不存在");
        }

        for (User item : result) {
            item.setPassword(Base64Util.decode(item.getPassword()));
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: checkUser
     * @Description: 用户的账号查找
     * @param telephone
     * @Return: com.mose.recruitment.model.send.MyResult<com.mose.recruitment.model.User>
     * @Author: mose
     * @Date: 2020/4/3 18:53
     */
    @PassToken
    @GetMapping("/user/check/{telephone}")
    public MyResult<User> checkUser(@PathVariable String telephone) {
        User result = userService.getUserByTelephone(telephone);
        if (result == null) {
            return ResultUtil.noExit("用户不存在");
        } else {
            return ResultUtil.success(result);
        }
    }

    /***
     *
     * @MethodName: getUserByPhoneAndPassword
     * @Description: 用户的登录认证
     * @param user
     * @Return: com.mose.recruitment.model.send.MyResult<com.mose.recruitment.model.User>
     * @Author: mose
     * @Date: 2020/4/3 18:53
     */
    @PassToken
    @PostMapping("/user/login")
    public MyResult<User> getUserByPhoneAndPassword(@RequestBody User user) {
        user.setPassword(Base64Util.encode(user.getPassword()));
        User userResult = userService.getUserByPhoneAndPassword(user.getTelephone(), user.getPassword());
        if (userResult == null) {
            return ResultUtil.noExit("用户不存在");
        }
        Map result = new HashMap();
        result.put("token", tokenService.getToken(userResult));
        result.put("user", userResult);

        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: user
     * @Description: 用户信息的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @UserLoginToken
    @GetMapping("/user/{id}")
    public MyResult<User> getUser(@PathVariable Integer id) {
        User result = userService.getUser(id);

        if (result == null) {
            return ResultUtil.noExit("获取用户失败");
        }
        result.setPassword(Base64Util.decode(result.getPassword()));
        return ResultUtil.success(result);
    }

    @PassToken
    @PostMapping("/user")
    public MyResult<Void> addUser(@RequestBody User user) {
        user.setPassword(Base64Util.encode(user.getPassword()));
        User userResult = userService.getUserByTelephone(user.getTelephone());
        if (userResult != null) {
            return ResultUtil.success();
        }
        if (userService.addUser(user) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/user")
    public MyResult<Void> updateUser(@RequestBody User user) {
        user.setPassword(Base64Util.encode(user.getPassword()));
        if (userService.updateUser(user) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/user/{id}")
    public MyResult<Void> deleteUser(@PathVariable Integer id) {
        if (userService.deleteUser(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
