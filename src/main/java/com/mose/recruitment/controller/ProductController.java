package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.PassToken;
import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Product;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.ProductService;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName ProductController
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/9
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    ProductService productService;

    /***
     *
     * @MethodName: getProductListByCompany
     * @Description: 根据公司获取其所有产品
     * @param company_id
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List<com.mose.recruitment.model.Product>>
     * @Author: mose
     * @Date: 2020/4/9 20:55
     */
    @PassToken
    @GetMapping("/products/{company_id}")
    public MyResult<List<Product>> getProductListByCompany(@PathVariable Integer company_id) {
        List<Product> result = productService.getProductListByCompany(company_id);

        if (result == null) {
            return ResultUtil.noExit("获取产品列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: product
     * @Description: 公司产品的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @PassToken
    @GetMapping("/product/{id}")
    public MyResult<Product> getProduct(@PathVariable Integer id) {
        Product result = productService.getProduct(id);

        if (result == null) {
            return ResultUtil.noExit("获取产品失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/product")
    public MyResult<Void> addProduct(@RequestBody Product product) {
        if (productService.addProduct(product) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/product")
    public MyResult<Void> updateProduct(@RequestBody Product product) {
        if (productService.updateProduct(product) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/product/{id}")
    public MyResult<Void> deleteProduct(@PathVariable Integer id) {
        if (productService.deleteProduct(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
