package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.model.send.NumberTrend;
import com.mose.recruitment.service.TrendService;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName TrendController
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/13
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class TrendController {

    @Autowired
    TrendService trendService;

    @UserLoginToken
    @GetMapping("/trend/apply")
    public MyResult<List<NumberTrend>> getApplyNumberTrendByDay() {
        List<NumberTrend> result = trendService.getApplyNumberTrendByDay();

        if (result == null) {
            return ResultUtil.noExit("获取近7天简历投递情况失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @GetMapping("/trend/position")
    public MyResult<List<NumberTrend>> getPositionNumberTrend() {
        List<NumberTrend> result = trendService.getPositionNumberTrend();

        if (result == null) {
            return ResultUtil.noExit("获取近7天简历投递情况失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @GetMapping("/trend/city")
    public MyResult<List<NumberTrend>> getCityNumberTrend() {
        List<NumberTrend> result = trendService.getCityNumberTrend();

        if (result == null) {
            return ResultUtil.noExit("获取近7天简历投递情况失败");
        }
        return ResultUtil.success(result);
    }
}
