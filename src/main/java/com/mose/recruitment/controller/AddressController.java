package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.PassToken;
import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Address;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.AddressService;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName AddressController
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/9
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class AddressController {

    @Autowired
    AddressService addressService;

    /***
     *
     * @MethodName: getAddressListByCompany
     * @Description: 根据公司获取其地址
     * @param company_id
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List<com.mose.recruitment.model.Address>>
     * @Author: mose
     * @Date: 2020/4/9 20:55
     */
    @PassToken
    @GetMapping("/addresses/{company_id}")
    public MyResult<List<Address>> getAddressListByCompany(@PathVariable Integer company_id) {
        List<Address> result = addressService.getAddressListByCompany(company_id);

        if (result == null) {
            return ResultUtil.noExit("获取产品列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: address
     * @Description: 公司地址的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @PassToken
    @GetMapping("/address/{id}")
    public MyResult<Address> getAddress(@PathVariable Integer id) {
        Address result = addressService.getAddress(id);

        if (result == null) {
            return ResultUtil.noExit("获取产品失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/address")
    public MyResult<Void> addAddress(@RequestBody Address address) {
        if (addressService.addAddress(address) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/address")
    public MyResult<Void> updateAddress(@RequestBody Address address) {
        if (addressService.updateAddress(address) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/address/{id}")
    public MyResult<Void> deleteAddress(@PathVariable Integer id) {
        if (addressService.deleteAddress(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
