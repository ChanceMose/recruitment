package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.PassToken;
import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Belong;
import com.mose.recruitment.model.Position;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.BelongService;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName BelongController
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/28
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class BelongController {

    @Autowired
    BelongService belongService;

    /***
     *
     * @MethodName: getBelongByPosition
     * @Description: 根据职位获取hr
     * @Param id
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @PassToken
    @GetMapping("/belong/position/{id}")
    public MyResult<Belong> getBelongByPosition(@PathVariable Integer id) {
        Belong result = belongService.getBelongByPosition(id);

        if (result == null) {
            return ResultUtil.noExit("获取HR失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getBelongsByCompanyId
     * @Description: 根据公司id获取旗下HR
     * @param company_id
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List       <       com.mose.recruitment.model.Belong>>
     * @Author: mose
     * @Date: 2020/4/3 17:55
     */
    @UserLoginToken
    @GetMapping("/belongs/{company_id}")
    public MyResult<List<Belong>> getBelongsByCompanyId(@PathVariable Integer company_id) {
        List<Belong> result = belongService.getBelongsByCompanyId(company_id);

        if (result == null) {
            return ResultUtil.noExit("获取HR列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: belongs
     * @Description: HR的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @UserLoginToken
    @GetMapping("/belong/{id}")
    public MyResult<Belong> getBelong(@PathVariable Integer id) {
        Belong result = belongService.getBelong(id);

        if (result == null) {
            return ResultUtil.noExit("用户非HR");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/belong")
    public MyResult<Void> addBelong(@RequestBody Belong belong) {
        if (belongService.addBelong(belong) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/belong")
    public MyResult<Void> updateBelong(@RequestBody Belong belong) {
        if (belongService.updateBelong(belong) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/belong/{id}")
    public MyResult<Void> deleteBelong(@PathVariable Integer id) {
        if (belongService.deleteBelong(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
