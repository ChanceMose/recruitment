package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.config.StatusCode;
import com.mose.recruitment.model.resume.BaseInfo;
import com.mose.recruitment.model.resume.Resume;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.UploadService;
import com.mose.recruitment.utils.ResultUtil;
import javassist.expr.Instanceof;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Null;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

/**
 * @ClassName UploadController
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/31
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class UploadController {

    @Value("${real-image-path}")
    private String UPLOAD_IMG_FOLDER;
    @Value("${real-file-path}")
    private String UPLOAD_FILE_FOLDER;

    @Autowired
    UploadService uploadService;

    /***
     *
     * @MethodName: uploadImg
     * @Description: 图片上传
     * @param file
     * @param type
     * @param id
     * @Return: com.mose.recruitment.model.send.MyResult<com.mose.recruitment.model.resume.BaseInfo>
     * @Author: mose
     * @Date: 2020/4/3 18:52
     */
    @UserLoginToken
    @PostMapping("/upload/image")
    public MyResult<BaseInfo> uploadImg(@RequestParam("file") MultipartFile file, @RequestParam("type") String type, @RequestParam("id") Integer id) throws IOException {
        if (Objects.isNull(file) || file.isEmpty()) {
            return ResultUtil.noExit("上传数据为空");
        }

        byte[] bytes = file.getBytes();
        String imgName = UUID.randomUUID().toString().replace("-", "") + ".png";
        Path path = Paths.get(UPLOAD_IMG_FOLDER + type + "/", imgName);
        // 如果没有files文件夹，则创建
        if (!Files.isWritable(path)) {
            Files.createDirectories(Paths.get(UPLOAD_IMG_FOLDER + type + "/"));
        }
        Files.write(path, bytes);
        // 删除原有图片
        if (id != 0) {
            if (!uploadService.getImage(type, id).isEmpty()) {
                Files.delete(Paths.get(UPLOAD_IMG_FOLDER + uploadService.getImage(type, id)));
            }
            if (uploadService.updateImage(type, type + "/" + imgName, id) > 0) {
                return ResultUtil.success(uploadService.getImage(type, id));
            }
        } else {
            return ResultUtil.success(type + "/" + imgName);
        }
        return ResultUtil.error(StatusCode.ERROR);
    }

    /***
     *
     * @MethodName: uploadFile
     * @Description: 文件上传
     * @param file
     * @param id
     * @Return: com.mose.recruitment.model.send.MyResult<com.mose.recruitment.model.resume.Resume>
     * @Author: mose
     * @Date: 2020/4/3 18:53
     */
    @UserLoginToken
    @PostMapping("/upload/file")
    public MyResult<Resume> uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("id") Integer id) throws IOException {
        if (Objects.isNull(file) || file.isEmpty()) {
            return ResultUtil.noExit("上传数据为空");
        }
        byte[] bytes = file.getBytes();
        String fileName = UUID.randomUUID().toString().replace("-", "") + ".pdf";
        Path path = Paths.get(UPLOAD_FILE_FOLDER, fileName);
        // 如果没有files文件夹，则创建
        if (!Files.isWritable(path)) {
            Files.createDirectories(Paths.get(UPLOAD_FILE_FOLDER));
        }
        Files.write(path, bytes);
        // 删除原有文件
        if (!uploadService.getFile(id).isEmpty()) {
            Files.delete(Paths.get(UPLOAD_FILE_FOLDER + uploadService.getFile(id)));
        }
        if (uploadService.updateFile(fileName, id) > 0) {
            return ResultUtil.success(uploadService.getFile(id));
        }
        return ResultUtil.error(StatusCode.ERROR);
    }
}
