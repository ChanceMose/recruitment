package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.PassToken;
import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Company;
import com.mose.recruitment.model.receive.ComFilterModel;
import com.mose.recruitment.model.send.CompanyList;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.CompanyService;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName CompanyController
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/10
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class CompanyController {

    @Autowired
    CompanyService companyService;

    /***
     *
     * @MethodName: getCompanyByPosition
     * @Description: 获取公司全名列表
     * @param
     * @Return: com.mose.recruitment.model.send.MyResult<com.mose.recruitment.model.Company>
     * @Author: mose
     * @Date: 2020/4/5 14:27
     */
    @PassToken
    @GetMapping("/company/position/{id}")
    public MyResult<Company> getCompanyByPosition(@PathVariable Integer id) {
        Company result = companyService.getCompanyByPosition(id);

        if (result == null) {
            return ResultUtil.noExit("获取公司失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getCompanyNameList
     * @Description: 获取公司全名列表
     * @param
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List               <               com.mose.recruitment.model.Company>>
     * @Author: mose
     * @Date: 2020/4/5 14:27
     */
    @PassToken
    @GetMapping("/companies/name")
    public MyResult<List<Company>> getCompanyNameList() {
        List<Company> result = companyService.getCompanyNameList();

        if (result == null) {
            return ResultUtil.noExit("获取公司全名列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getCompanyList
     * @Description: 获取公司列表
     * @param
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List                               <                               com.mose.recruitment.model.send.CompanyList>>
     * @Author: mose
     * @Date: 2020/4/3 17:53
     */
    @PassToken
    @GetMapping("/companies")
    public MyResult<List<CompanyList>> getCompanyList() {
        List<CompanyList> result = companyService.getCompanyList();

        if (result == null) {
            return ResultUtil.noExit("获取公司列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getCompanyListBySearch
     * @Description: 根据搜索条件模糊搜索公司
     * @param search
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List                               <                               com.mose.recruitment.model.send.CompanyList>>
     * @Author: mose
     * @Date: 2020/4/3 17:52
     */
    @PassToken
    @GetMapping("/companies/{search}")
    public MyResult<List<CompanyList>> getCompanyListBySearch(@PathVariable String search) {
        search = "%" + search + "%";
        List<CompanyList> result = companyService.getCompanyListBySearch(search);

        if (result == null) {
            return ResultUtil.noExit("获取公司列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getCompanyListByFilter
     * @Description: 根据过滤条件过滤公司
     * @param comFilterModel
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List                               <                               com.mose.recruitment.model.send.CompanyList>>
     * @Author: mose
     * @Date: 2020/4/3 17:52
     */
    @PassToken
    @PostMapping("/companies/filterValue")
    public MyResult<List<CompanyList>> getCompanyListByFilter(@RequestBody ComFilterModel comFilterModel) {
        String city = comFilterModel.getCity();
        String finance = comFilterModel.getFinance();
        String companySize = comFilterModel.getCompanySize();
        String field = comFilterModel.getField();
        if (city.equals("全国"))
            city = "";
        if (finance.equals("不限"))
            finance = "";
        if (companySize.equals("不限"))
            companySize = "";
        if (field.equals("不限"))
            field = "";
        else field = "%" + field + "%";
        List<CompanyList> result = companyService.getCompanyListByFilter(city, finance, companySize, field);

        if (result == null) {
            return ResultUtil.noExit("获取公司列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: company
     * @Description: 公司信息的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @PassToken
    @GetMapping("/company/{id}")
    public MyResult<Company> getCompany(@PathVariable Integer id) {
        Company result = companyService.getCompany(id);

        if (result == null) {
            return ResultUtil.noExit("获取公司详情失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/company")
    public MyResult<Void> addCompany(@RequestBody Company company) {
        if (companyService.addCompany(company) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/company")
    public MyResult<Void> updateCompany(@RequestBody Company company) {
        if (companyService.updateCompany(company) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/company/{id}")
    public MyResult<Void> deleteCompany(@PathVariable Integer id) {
        if (companyService.deleteCompany(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
