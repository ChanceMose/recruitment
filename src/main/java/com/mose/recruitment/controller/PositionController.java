package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.PassToken;
import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Position;
import com.mose.recruitment.model.send.PositionList;
import com.mose.recruitment.model.receive.PositionFilter;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.PositionService;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName RecruitInfoController
 * @Description: TODO
 * @Author mose
 * @Date 2020/2/28
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class PositionController {

    @Autowired
    PositionService positionService;

    /***
     *
     * @MethodName: getPositionListByCompany
     * @Description: 根据公司id获取其所有在招职位
     * @param company_id
     * @param state
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List                                                                                                                                                                                                                                                               <                                                                                                                                                                                                                                                               com.mose.recruitment.model.Position>>
     * @Author: mose
     * @Date: 2020/4/5 17:32
     */
    @PassToken
    @GetMapping("/positions/{company_id}/{state}")
    public MyResult<List<Position>> getPositionListByCompany(@PathVariable Integer company_id, @PathVariable Integer state) {
        List<Position> result = positionService.getPositionListByCompany(company_id, state);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    @PassToken
    @GetMapping("/positions")
    public MyResult<List<PositionList>> getPositionList(@RequestParam String query) {
        query = "%" + query + "%";
        List<PositionList> result = positionService.getPositionList(query);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getPositionNameListByCompany
     * @Description: 根据HRid获取其所有在招职位名称
     * @param hr_id
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List                                                                                                                                                                                                                                                               <                                                                                                                                                                                                                                                               com.mose.recruitment.model.Position>>
     * @Author: mose
     * @Date: 2020/4/5 17:32
     */
    @UserLoginToken
    @GetMapping("/positions/{hr_id}/state/{state}")
    public MyResult<List<Position>> getPositionListByState(@PathVariable Integer hr_id, @PathVariable Integer state) {
        List<Position> result = positionService.getPositionListByState(hr_id, state);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getPositionNameListByCompany
     * @Description: 根据公司id获取其所有在招职位名称
     * @param company_id
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List                                                                                                                                                                                                                                                               <                                                                                                                                                                                                                                                               com.mose.recruitment.model.Position>>
     * @Author: mose
     * @Date: 2020/4/5 17:32
     */
    @UserLoginToken
    @GetMapping("/positionNames/{company_id}")
    public MyResult<List<Position>> getPositionNameListByCompany(@PathVariable Integer company_id) {
        List<Position> result = positionService.getPositionNameListByCompany(company_id);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getPositionListByLimitNumber
     * @Description: 根据数量获取职位
     * @param number
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               com.mose.recruitment.model.send.PositionList>>
     * @Author: mose
     * @Date: 2020/4/3 18:03
     */
    @PassToken
    @GetMapping("/positions/{number}")
    public MyResult<List<PositionList>> getPositionListByLimitNumber(@PathVariable Integer number) {
        List<PositionList> result = positionService.getPositionListByLimitNumber(number);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @MethodName: getPositionListByFilter
     * @Description: 根据过滤信息获取职位
     * @param filter
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               com.mose.recruitment.model.send.PositionList>>
     * @Author: mose
     * @Date: 2020/4/3 18:02
     */
    @PassToken
    @PostMapping("/positions/filterValue")
    public MyResult<List<PositionList>> getPositionListByFilter(@RequestBody PositionFilter filter) {
        String search = '%' + filter.getSearch() + '%';
        String city = filter.getCity();
        String district = filter.getDistrict();
        String experience = filter.getExperience();
        String education = filter.getEducation();
        String salary = filter.getSalary();
        int salary_min;
        int salary_max;
        String finance = filter.getFinance();
        String companySize = filter.getCompanySize();
        String field = filter.getField();
        if (city.equals("全国"))
            city = "";
        if (district.equals("不限"))
            district = "";
        if (experience.equals("不限"))
            experience = "";
        else if (experience.equals("不要求"))
            experience = "不限";
        if (education.equals("不限"))
            education = "";
        else if (education.equals("不要求"))
            education = "不限";
        if (salary.equals("不限")) {
            salary_min = -1;
            salary_max = -1;
        } else if (salary.equals("2k以下")) {
            salary_min = 0;
            salary_max = 2;
        } else if (salary.equals("50k以上")) {
            salary_min = 50;
            salary_max = 1000;
        } else {
            String[] lists = salary.split("-");
            salary_min = Integer.parseInt(lists[0].substring(0, lists[0].length() - 1));
            salary_max = Integer.parseInt(lists[1].substring(0, lists[1].length() - 1));
        }
        if (district.equals("不限"))
            district = "";
        if (finance.equals("不限"))
            finance = "";
        if (companySize.equals("不限"))
            companySize = "";
        if (field.equals("不限"))
            field = "";
        else field = "%" + field + "%";
        List<PositionList> result = positionService.getPositionListByFilter(search, city, district, experience, education, salary_min, salary_max, finance, companySize, field);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: position
     * @Description: 职位信息的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @PassToken
    @GetMapping("/position/{id}")
    public MyResult<Position> getPosition(@PathVariable Integer id) {
        Position result = positionService.getPosition(id);

        if (result == null) {
            return ResultUtil.noExit("获取职位详情失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/position")
    public MyResult<Void> addPosition(@RequestBody Position position) {
        if (positionService.addPosition(position) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/position")
    public MyResult<Void> updatePosition(@RequestBody Position position) {
        if (positionService.updatePosition(position) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/position/{id}")
    public MyResult<Void> deletePosition(@PathVariable Integer id) {
        if (positionService.deletePosition(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
