package com.mose.recruitment.controller;

import com.mose.recruitment.annotation.PassToken;
import com.mose.recruitment.annotation.UserLoginToken;
import com.mose.recruitment.model.Job;
import com.mose.recruitment.model.JobTree;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.service.JobService;
import com.mose.recruitment.utils.ResultUtil;
import com.mose.recruitment.utils.TreeParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName JobController
 * @Description: 工作控制类
 * @Author mose
 * @Date 2020/2/23
 * @Version V1.0
 **/
@RestController
@RequestMapping("/api")
public class JobController {

    @Autowired
    JobService jobService;

    /***
     *
     * @MethodName: getJobTreeList
     * @Description: 获取职业类别树
     * @param
     * @Return: com.mose.recruitment.model.send.MyResult<java.util.List<com.mose.recruitment.model.JobTree>>
     * @Author: mose
     * @Date: 2020/4/3 18:44
     */
    @PassToken
    @GetMapping("/jobs/tree")
    public MyResult<List<JobTree>> getJobTreeList() {
        List<JobTree> jobList = jobService.getJobByAll();
        List<JobTree> result = TreeParser.getTreeList(0, jobList);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    @PassToken
    @GetMapping("/jobs/{level}")
    public MyResult<List<Job>> getJobListByLevel(@PathVariable Integer level) {
        List<Job> result = jobService.getJobListByLevel(level);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    @PassToken
    @GetMapping("/jobs")
    public MyResult<List<Job>> getJobList(@RequestParam String query) {
        query = "%" + query + "%";
        List<Job> result = jobService.getJobList(query);

        if (result == null) {
            return ResultUtil.noExit("获取职位列表失败");
        }
        return ResultUtil.success(result);
    }

    /***
     *
     * @Name: job
     * @Description: 职位类别的crud
     * @Author: mose
     * @Date: 2020/4/1 22:56
     */
    @PassToken
    @GetMapping("/job/{id}")
    public MyResult<Job> getJob(@PathVariable Integer id) {
        Job result = jobService.getJob(id);

        if (result == null) {
            return ResultUtil.noExit("获取职位类别详情失败");
        }
        return ResultUtil.success(result);
    }

    @UserLoginToken
    @PostMapping("/job")
    public MyResult<Void> addJob(@RequestBody Job job) {
        if (jobService.addJob(job) > 0) {
            return ResultUtil.successUpdate("添加成功");
        } else {
            return ResultUtil.failUpdate("添加失败");
        }
    }

    @UserLoginToken
    @PutMapping("/job")
    public MyResult<Void> updateJob(@RequestBody Job job) {
        if (jobService.updateJob(job) > 0) {
            return ResultUtil.successUpdate("修改成功");
        } else {
            return ResultUtil.failUpdate("修改失败");
        }
    }

    @UserLoginToken
    @DeleteMapping("/job/{id}")
    public MyResult<Void> deleteJob(@PathVariable Integer id) {
        if (jobService.deleteJob(id) > 0) {
            return ResultUtil.successDel();
        } else {
            return ResultUtil.failDel();
        }
    }
}
