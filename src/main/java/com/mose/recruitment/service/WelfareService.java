package com.mose.recruitment.service;

import com.mose.recruitment.Dao.WelfareDao;
import com.mose.recruitment.model.Welfare;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName WelfareService
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/9
 * @Version V1.0
 **/
@Service
public class WelfareService {

    @Resource
    WelfareDao welfareDao;

    public List<Welfare> getWelfareListByCompany(int company_id) {
        return welfareDao.getWelfareListByCompany(company_id);
    }

    public Welfare getWelfare(int id) {
        return welfareDao.getWelfare(id);
    }

    public int addWelfare(Welfare welfare) {
        return welfareDao.addWelfare(welfare);
    }

    public int updateWelfare(Welfare welfare) {
        return welfareDao.updateWelfare(welfare);
    }

    public int deleteWelfare(int id) {
        return welfareDao.deleteWelfare(id);
    }
}
