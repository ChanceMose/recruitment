package com.mose.recruitment.service;

import com.mose.recruitment.Dao.ResumeDao;
import com.mose.recruitment.model.resume.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName ResumeService
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/1
 * @Version V1.0
 **/
@Service
public class ResumeService {

    @Resource
    ResumeDao resumeDao;

    public List<ResumeList> getResumeListByFilter(int company_id) {
        return resumeDao.getResumeListByFilter(company_id);
    }

    public List<ResumeList> getResumeListByPosition(String position, int company_id) {
        return resumeDao.getResumeListByPosition(position, company_id);
    }

    // 基础信息
    public BaseInfo getBaseInfo(int id) {
        return resumeDao.getBaseInfo(id);
    }

    public int addBaseInfo(BaseInfo baseInfo) {
        return resumeDao.addBaseInfo(baseInfo);
    }

    public int updateBaseInfo(BaseInfo baseInfo) {
        return resumeDao.updateBaseInfo(baseInfo);
    }

    public int deleteBaseInfo(int id) {
        return resumeDao.deleteBaseInfo(id);
    }

    // 简历信息
    public Resume getResume(int id) {
        return resumeDao.getResume(id);
    }

    public int addResume(Resume resume) {
        return resumeDao.addResume(resume);
    }

    public int updateResume(Resume resume) {
        return resumeDao.updateResume(resume);
    }

    public int deleteResume(int id) {
        return resumeDao.deleteResume(id);
    }

    // 教育经历
    public List<Education> getEducationList(int user_id) {
        return resumeDao.getEducationList(user_id);
    }

    public int addEducation(Education education) {
        return resumeDao.addEducation(education);
    }

    public int updateEducation(Education education) {
        return resumeDao.updateEducation(education);
    }

    public int deleteEducation(int id) {
        return resumeDao.deleteEducation(id);
    }

    // 项目经历
    public List<Work> getWorkList(int user_id) {
        return resumeDao.getWorkList(user_id);
    }

    public int addWork(Work work) {
        return resumeDao.addWork(work);
    }

    public int updateWork(Work work) {
        return resumeDao.updateWork(work);
    }

    public int deleteWork(int id) {
        return resumeDao.deleteWork(id);
    }

    // 项目经历
    public List<Project> getProjectList(int user_id) {
        return resumeDao.getProjectList(user_id);
    }

    public int addProject(Project project) {
        return resumeDao.addProject(project);
    }

    public int updateProject(Project project) {
        return resumeDao.updateProject(project);
    }

    public int deleteProject(int id) {
        return resumeDao.deleteProject(id);
    }

    // 求职意向
    public List<Intention> getIntentionList(int user_id) {
        return resumeDao.getIntentionList(user_id);
    }

    public int addIntention(Intention intention) {
        return resumeDao.addIntention(intention);
    }

    public int updateIntention(Intention intention) {
        return resumeDao.updateIntention(intention);
    }

    public int deleteIntention(int id) {
        return resumeDao.deleteIntention(id);
    }
}
