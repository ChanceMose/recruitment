package com.mose.recruitment.service;

import com.mose.recruitment.Dao.ConditionDao;
import com.mose.recruitment.model.Address;
import com.mose.recruitment.model.Condition;
import com.mose.recruitment.model.Position;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName ConditionService
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/6
 * @Version V1.0
 **/
@Service
public class ConditionService {

    @Resource
    ConditionDao conditionDao;

    public List<Condition> getConditionByState(int state) {
        return conditionDao.getConditionByState(state);
    }

    public List<Position> getCityListInPosition() {
        return conditionDao.getCityListInPosition();
    }

    public List<Address> getCityListInCompany() {
        return conditionDao.getCityListInCompany();
    }

    public List<Position> getDistrictListByCity(String city) {
        return conditionDao.getDistrictListByCity(city);
    }

    public Condition getCondition(int id) {
        return conditionDao.getCondition(id);
    }

    public int addCondition(Condition condition) {
        return conditionDao.addCondition(condition);
    }

    public int updateCondition(Condition condition) {
        return conditionDao.updateCondition(condition);
    }

    public int deleteCondition(int id) {
        return conditionDao.deleteCondition(id);
    }
}
