package com.mose.recruitment.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.mose.recruitment.model.User;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @ClassName TokenService
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/13
 * @Version V1.0
 **/
@Service
public class TokenService {

    public String getToken(User user) {
        Date start = new Date();
        long currentTime = System.currentTimeMillis() + 60 * 60 * 1000;//一小时有效时间
        Date end = new Date(currentTime);
        String token;

        // Algorithm.HMAC256():使用HS256生成token,密钥则是用户的密码，唯一密钥的话可以保存在服务端。
        // withAudience()存入需要保存在token的信息，这里我把用户ID和token持续时间存入token中
            token = JWT.create().withAudience(user.getId().toString()).withIssuedAt(start).withExpiresAt(end)
                .sign(Algorithm.HMAC256(user.getPassword()));
        return token;
    }
}
