package com.mose.recruitment.service;

import com.mose.recruitment.Dao.BelongDao;
import com.mose.recruitment.model.Belong;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName BelongService
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/28
 * @Version V1.0
 **/
@Service
public class BelongService {

    @Resource
    BelongDao belongDao;

    public Belong getBelongByPosition(int id) {
        return belongDao.getBelongByPosition(id);
    }

    public List<Belong> getBelongsByCompanyId(int company_id) {
        return belongDao.getBelongsByCompanyId(company_id);
    }

    public Belong getBelong(int id) {
        return belongDao.getBelong(id);
    }

    public int addBelong(Belong belong) {
        return belongDao.addBelong(belong);
    }

    public int updateBelong(Belong belong) {
        return belongDao.updateBelong(belong);
    }

    public int deleteBelong(int id) {
        return belongDao.deleteBelong(id);
    }
}
