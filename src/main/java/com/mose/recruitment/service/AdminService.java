package com.mose.recruitment.service;

import com.mose.recruitment.Dao.AdminDao;
import com.mose.recruitment.model.Admin;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @ClassName AdminService
 * @Description: TODO
 * @Author mose
 * @Date 2020/1/21
 * @Version V1.0
 **/
@Service
public class AdminService {

    @Resource
    AdminDao adminDao;

    public Admin getAdminByNameAndPassword(Admin admin) {
        return adminDao.getAdminByNameAndPassword(admin);
    }

    public Admin getAdmin(int id) {
        return adminDao.getAdmin(id);
    }

    public int addAdmin(Admin admin) {
        return adminDao.addAdmin(admin);
    }

    public int updateAdmin(Admin admin) {
        return adminDao.updateAdmin(admin);
    }

    public int deleteAdmin(int id) {
        return adminDao.deleteAdmin(id);
    }
}
