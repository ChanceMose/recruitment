package com.mose.recruitment.service;

import com.mose.recruitment.Dao.ProductDao;
import com.mose.recruitment.model.Product;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName ProductService
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/9
 * @Version V1.0
 **/
@Service
public class ProductService {
    @Resource
    ProductDao productDao;

    public List<Product> getProductListByCompany(int company_id) {
        return productDao.getProductListByCompany(company_id);
    }

    public Product getProduct(int id) {
        return productDao.getProduct(id);
    }

    public int addProduct(Product product) {
        return productDao.addProduct(product);
    }

    public int updateProduct(Product product) {
        return productDao.updateProduct(product);
    }

    public int deleteProduct(int id) {
        return productDao.deleteProduct(id);
    }
}
