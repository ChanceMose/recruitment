package com.mose.recruitment.service;

import com.mose.recruitment.Dao.TypeDao;
import com.mose.recruitment.model.Job;
import com.mose.recruitment.model.Type;
import com.mose.recruitment.model.TypeTree;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName TypeService
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/14
 * @Version V1.0
 **/
@Service
public class TypeService {

    @Resource
    TypeDao typeDao;

    public List<Type> getTypeListByLevel(int level) {
        return typeDao.getTypeListByLevel(level);
    }

    public List<TypeTree> getTypeAll() {
        return typeDao.getTypeAll();
    }

    public List<Type> getTypeList(String query) {
        return typeDao.getTypeList(query);
    }

    public Type getType(int id) {
        return typeDao.getType(id);
    }

    public int addType(Type type) {
        return typeDao.addType(type);
    }

    public int updateType(Type type) {
        return typeDao.updateType(type);
    }

    public int deleteType(int id) {
        return typeDao.deleteType(id);
    }
}
