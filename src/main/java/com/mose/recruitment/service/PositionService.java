package com.mose.recruitment.service;

import com.mose.recruitment.Dao.PositionDao;
import com.mose.recruitment.model.Position;
import com.mose.recruitment.model.send.PositionList;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName RecruitInfoService
 * @Description: TODO
 * @Author mose
 * @Date 2020/2/28
 * @Version V1.0
 **/
@Service
public class PositionService {

    @Resource
    PositionDao positionDao;

    public List<Position> getPositionListByCompany(int company_id, int state) {
        return positionDao.getPositionListByCompany(company_id, state);
    }

    public List<PositionList> getPositionList(String query) {
        return positionDao.getPositionList(query);
    }

    public List<Position> getPositionListByState(int hr_id, int state) {
        return positionDao.getPositionListByState(hr_id, state);
    }

    public List<Position> getPositionNameListByCompany(int company_id) {
        return positionDao.getPositionNameListByCompany(company_id);
    }

    public List<PositionList> getPositionListByLimitNumber(int number) {
        return positionDao.getPositionListByLimitNumber(number);
    }

    public List<PositionList> getPositionListByFilter(String search, String city, String district, String experience, String education, int salary_min, int salary_max, String finance, String companySize, String field) {
        return positionDao.getPositionListByFilter(search, city, district, experience, education, salary_min, salary_max, finance, companySize, field);
    }

    public Position getPosition(int id) {
        return positionDao.getPosition(id);
    }

    public int addPosition(Position position) {
        return positionDao.addPosition(position);
    }

    public int updatePosition(Position position) {
        return positionDao.updatePosition(position);
    }

    public int deletePosition(int id) {
        return positionDao.deletePosition(id);
    }
}
