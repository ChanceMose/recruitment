package com.mose.recruitment.service;

import com.mose.recruitment.Dao.AddressDao;
import com.mose.recruitment.model.Address;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName AddressService
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/9
 * @Version V1.0
 **/
@Service
public class AddressService {

    @Resource
    AddressDao addressDao;

    public List<Address> getAddressListByCompany(int company_id) {
        return addressDao.getAddressListByCompany(company_id);
    }

    public Address getAddress(int id) {
        return addressDao.getAddress(id);
    }

    public int addAddress(Address address) {
        return addressDao.addAddress(address);
    }

    public int updateAddress(Address address) {
        return addressDao.updateAddress(address);
    }

    public int deleteAddress(int id) {
        return addressDao.deleteAddress(id);
    }
}
