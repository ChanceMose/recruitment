package com.mose.recruitment.service;

import com.mose.recruitment.Dao.UserDao;
import com.mose.recruitment.model.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName UserService
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/12
 * @Version V1.0
 **/
@Service
public class UserService {
    @Resource
    UserDao userDao;

    public List<User> getUserListByQuery(String query) {
        return userDao.getUserListByQuery(query);
    }

    public User getUserByTelephone(String telephone) {
        return userDao.getUserByTelephone(telephone);
    }

    public User getUserByPhoneAndPassword(String telephone, String password) {
        return userDao.getUserByPhoneAndPassword(telephone, password);
    }

    public User getUser(int id) {
        return userDao.getUser(id);
    }

    public int addUser(User user) {
        return userDao.addUser(user);
    }

    public int updateUser(User user) {
        return userDao.updateUser(user);
    }

    public int deleteUser(int id) {
        return userDao.deleteUser(id);
    }
}
