package com.mose.recruitment.service;

import com.mose.recruitment.Dao.JobDao;
import com.mose.recruitment.model.Job;
import com.mose.recruitment.model.JobTree;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName JobService
 * @Description: TODO
 * @Author mose
 * @Date 2020/2/23
 * @Version V1.0
 **/
@Service
public class JobService {

    @Resource
    JobDao jobDao;

    public List<Job> getJobListByLevel(int level) {
        return jobDao.getJobListByLevel(level);
    }

    public List<Job> getJobList(String query) {
        return jobDao.getJobList(query);
    }

    public List<JobTree> getJobByAll() {
        return jobDao.getJobByAll();
    }

    public Job getJob(int id) {
        return jobDao.getJob(id);
    }

    public int addJob(Job job) {
        return jobDao.addJob(job);
    }

    public int updateJob(Job job) {
        return jobDao.updateJob(job);
    }

    public int deleteJob(int id) {
        return jobDao.deleteJob(id);
    }
}
