package com.mose.recruitment.service;

import com.mose.recruitment.Dao.ApplyDao;
import com.mose.recruitment.model.Apply;
import com.mose.recruitment.model.ApplyList;
import com.mose.recruitment.model.UserApplyList;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName ApplyService
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/7
 * @Version V1.0
 **/
@Service
public class ApplyService {

    @Resource
    ApplyDao applyDao;

    public List<UserApplyList> getApplyListByUserAndState(int resume_id, int state) {
        return applyDao.getApplyListByUserAndState(resume_id, state);
    }

    public Apply getApplyByUserAndHr(int resume_id, int hr_id, String position) {
        return applyDao.getApplyByUserAndHr(resume_id, hr_id, position);
    }

    public List<ApplyList> getApplyListByHrAndTwoState(int hr_id, int state1, int state2) {
        return applyDao.getApplyListByHrAndTwoState(hr_id, state1, state2);
    }

    public List<ApplyList> getApplyListByHr(int hr_id, int state) {
        return applyDao.getApplyListByHr(hr_id, state);
    }

    public Apply getApply(int id) {
        return applyDao.getApply(id);
    }

    public int addApply(Apply apply) {
        return applyDao.addApply(apply);
    }

    public int updateApply(Apply apply) {
        return applyDao.updateApply(apply);
    }

    public int deleteApply(int id) {
        return applyDao.deleteApply(id);
    }
}
