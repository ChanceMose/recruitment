package com.mose.recruitment.service;

import com.mose.recruitment.Dao.UploadDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @ClassName UploadService
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/31
 * @Version V1.0
 **/
@Service
public class UploadService {

    @Resource
    UploadDao uploadDao;

    public String getImage(String table, int id) {
        return uploadDao.getImage(table, id);
    }

    public int updateImage(String table, String image, int id) {
        return uploadDao.updateImage(table, image, id);
    }

    public String getFile(int id) {
        return uploadDao.getFile(id);
    }

    public int updateFile(String file, int id) {
        return uploadDao.updateFile(file, id);
    }
}
