package com.mose.recruitment.service;

import java.util.List;

/**
 * @ClassName TreeService
 * @Description: 树型数据实体接口
 * @Author mose
 * @Date 2020/2/23
 * @Version V1.0
 **/
public interface TreeService<E> {

    Integer getId();

    Integer getParentId();

    void setChildren(List<E> children);
}
