package com.mose.recruitment.service;

import com.mose.recruitment.Dao.TrendDao;
import com.mose.recruitment.model.send.NumberTrend;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName TrendService
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/13
 * @Version V1.0
 **/
@Service
public class TrendService {

    @Resource
    TrendDao trendDao;

    public List<NumberTrend> getApplyNumberTrendByDay() {
        return trendDao.getApplyNumberTrendByDay();
    }

    public List<NumberTrend> getPositionNumberTrend() {
        return trendDao.getPositionNumberTrend();
    }

    public List<NumberTrend> getCityNumberTrend() {
        return trendDao.getCityNumberTrend();
    }
}
