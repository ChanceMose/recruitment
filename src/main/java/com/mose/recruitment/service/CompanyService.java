package com.mose.recruitment.service;

import com.mose.recruitment.Dao.CompanyDao;
import com.mose.recruitment.model.Company;
import com.mose.recruitment.model.Position;
import com.mose.recruitment.model.send.CompanyList;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName CompanyServices
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/10
 * @Version V1.0
 **/
@Service
public class CompanyService {

    @Resource
    CompanyDao companyDao;

    public Company getCompanyByPosition(int id) {
        return companyDao.getCompanyByPosition(id);
    }

    public List<Company> getCompanyNameList() {
        return companyDao.getCompanyNameList();
    }

    public List<CompanyList> getCompanyList() {
        return companyDao.getCompanyList();
    }

    public List<CompanyList> getCompanyListBySearch(String search) {
        return companyDao.getCompanyListBySearch(search);
    }

    public List<CompanyList> getCompanyListByFilter(String city, String finance, String companySize, String field) {
        return companyDao.getCompanyListByFilter(city, finance, companySize, field);
    }

    public Company getCompany(int id) {
        return companyDao.getCompany(id);
    }

    public int addCompany(Company company) {
        return companyDao.addCompany(company);
    }

    public int updateCompany(Company company) {
        return companyDao.updateCompany(company);
    }

    public int deleteCompany(int id) {
        return companyDao.deleteCompany(id);
    }
}
