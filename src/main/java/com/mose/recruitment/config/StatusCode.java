package com.mose.recruitment.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @ClassName StatusCode
 * @Description: 常用错误码
 * @Author mose
 * @Date 2020/2/24
 * @Version V1.0
 **/
@Getter
@AllArgsConstructor
public enum StatusCode {

    ERROR(-1, "未知错误"),
    SUCCESS(200, "获取数据成功"),
    //用户新建或修改数据成功
    SUCCESS_UPDATE(201, "操作成功"),
    WAITING(202, "请求已经进入后台排队"),
    SUCCESS_DEL(204, "删除成功"),
    REQUEST_ERROR(400, "请求有错误，服务器没有进行新建或修改数据"),
    NOT_RIGHTS(401, "用户没有权限（令牌、用户名、密码错误）"),
    FORBID(403, "用户得到授权，但是访问是被禁止的"),
    NOT_EXIT(404, "访问资源不存在"),
    FORMAT_ERROR(406, "请求格式错误，例如请求JSON，只有XML格式"),
    NOT_FIND(410, "用户请求的资源被永久删除，且不会再得到的"),
    CREATE_ERROR(422, "当创建一个对象时，发生一个验证错误"),
    UNKNOWN_ERROR(500, "服务器发生错误");

    private Integer code;
    private String msg;
}
