package com.mose.recruitment.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * @ClassName StaticFileConfig
 * @Description: 虚拟路径映射配置类
 * @Author mose
 * @Date 2020/2/24
 * @Version V1.0
 **/
@Configuration
public class StaticFileConfig implements WebMvcConfigurer {

    @Value("${real-image-path}")
    private String UPLOAD_IMG_FOLDER;
    @Value("${real-file-path}")
    private String UPLOAD_FILE_FOLDER;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/images/**").addResourceLocations("file:" + UPLOAD_IMG_FOLDER);
        registry.addResourceHandler("/Files/**").addResourceLocations("file:" + UPLOAD_FILE_FOLDER);
        //阿里云(映射路径去除盘符)
        //registry.addResourceHandler("/ueditor/image/**").addResourceLocations("/upload/image/");
    }
}
