package com.mose.recruitment.config;

import com.mose.recruitment.model.send.MyException;
import com.mose.recruitment.model.send.MyResult;
import com.mose.recruitment.utils.ResultUtil;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName ExceptionHandle
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/17
 * @Version V1.0
 **/
@ControllerAdvice
public class ExceptionHandle {
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public MyResult handle(Exception e) {
        if (e instanceof MyException) {
            MyException myException = (MyException) e;
            return ResultUtil.error(myException);
        } else {
            return ResultUtil.error(-1, e.getMessage());
        }
    }
}
