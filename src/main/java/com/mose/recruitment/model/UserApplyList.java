package com.mose.recruitment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName UserApplyList
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/9
 * @Version V1.0
 **/
@Data
public class UserApplyList {
    private Integer id;
    private Integer resume_id;
    private Integer hr_id;
    private String position;
    // 前端到后端
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    // 后端到前端，timezone = "GMT+8"转换时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date date;
    private Integer state;
    private Integer position_id;
    private Integer salary_min;
    private Integer salary_max;
    private Integer company_id;
    private String shortName;
    private String city;
}
