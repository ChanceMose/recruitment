package com.mose.recruitment.model.send;

import com.mose.recruitment.config.StatusCode;
import lombok.Data;

/**
 * @ClassName MyException
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/17
 * @Version V1.0
 **/
@Data
public class MyException extends RuntimeException {

    /**
     * 我们希望定位的错误更准确，
     * 希望不同的错误可以返回不同的错误码，所以可以自定义一个Exception
     * <p>
     * 注意要继承自RuntimeException，底层RuntimeException继承了Exception,
     * spring框架只对抛出的异常是RuntimeException才会进行事务回滚，
     * 如果是抛出的是Exception，是不会进行事物回滚的
     */
    private Integer code;

    public MyException(StatusCode statusCode) {
        super(statusCode.getMsg());
        this.code = statusCode.getCode();
    }

    public MyException(StatusCode statusCode, String value) {
        super(value);
        this.code = statusCode.getCode();
    }

}
