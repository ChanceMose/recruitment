package com.mose.recruitment.model.send;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName CompanyList
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/10
 * @Version V1.0
 **/
@Data
public class CompanyList {
    private Integer id;
    private String shortName;
    private String fullName;
    private String type;
    private String image;
    private String word;
    private String financingType;
    private String population;
    private String homepage;
    private String labels;
    private String introduction;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createTime;
    private String capital;
    private String representative;
    private Integer legal;
    private String city;

}
