package com.mose.recruitment.model.send;

import lombok.Data;

/**
 * @ClassName MyResult
 * @Description: 统一返回数据格式
 * @Author mose
 * @Date 2020/2/24
 * @Version V1.0
 **/
@Data
public class MyResult<T> {
    //返回码
    private Integer code;
    //提示信息
    private String msg;
    //返回具体内容
    private T data;
}
