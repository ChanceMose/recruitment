package com.mose.recruitment.model.send;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName NumberTrend
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/13
 * @Version V1.0
 **/
@Data
public class NumberTrend {
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date date;
    private String position;
    private String city;
    private Integer number;
}
