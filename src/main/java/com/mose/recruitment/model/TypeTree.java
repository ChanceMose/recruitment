package com.mose.recruitment.model;

import com.mose.recruitment.service.TreeService;
import lombok.Data;

import java.util.List;

/**
 * @ClassName TypeTree
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/14
 * @Version V1.0
 **/
@Data
public class TypeTree implements TreeService<TypeTree> {
    private Integer id;
    private String name;
    private Integer parentId;
    private Integer level;
    public List<TypeTree> children;
}
