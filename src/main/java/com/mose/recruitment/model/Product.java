package com.mose.recruitment.model;

import lombok.Data;

/**
 * @ClassName Product
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/12
 * @Version V1.0
 **/
@Data
public class Product {
    private Integer id;
    private String name;
    private String introduction;
    private String image;
    private String url;
    private Integer company_id;
}
