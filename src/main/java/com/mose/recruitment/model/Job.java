package com.mose.recruitment.model;

import lombok.Data;

/**
 * @ClassName Job
 * @Description: 工作类别实体类
 * @Author mose
 * @Date 2020/2/23
 * @Version V1.0
 **/
@Data
public class Job {
    private Integer id;
    private String name;
    private Integer parentId;
    private Integer level;
}
