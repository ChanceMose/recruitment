package com.mose.recruitment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName Apply
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/7
 * @Version V1.0
 **/
@Data
public class Apply {
    private Integer id;
    private Integer resume_id;
    private Integer hr_id;
    private String position;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date date;
    private Integer state;
}
