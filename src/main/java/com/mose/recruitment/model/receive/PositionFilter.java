package com.mose.recruitment.model.receive;

import lombok.Data;

/**
 * @ClassName PositionFilter
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/8
 * @Version V1.0
 **/
@Data
public class PositionFilter {
    private String search;
    private String city;
    private String district;
    private String experience;
    private String education;
    private String salary;
    private String finance;
    private String companySize;
    private String field;
}
