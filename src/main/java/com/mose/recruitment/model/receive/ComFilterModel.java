package com.mose.recruitment.model.receive;

import lombok.Data;

/**
 * @ClassName ComFilterModel
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/11
 * @Version V1.0
 **/
@Data
public class ComFilterModel {
    private String city;
    private String finance;
    private String companySize;
    private String field;
}
