package com.mose.recruitment.model;

import lombok.Data;

/**
 * @ClassName Admin
 * @Description: 管理员实体类
 * @Author mose
 * @Date 2020/1/21
 * @Version V1.0
 **/
@Data
public class Admin {
    private Integer id;
    private String name;
    private String password;
}
