package com.mose.recruitment.model;

import lombok.Data;

/**
 * @ClassName Welfare
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/12
 * @Version V1.0
 **/
@Data
public class Welfare {
    private Integer id;
    private String name;
    private String detail;
    private Integer company_id;
}
