package com.mose.recruitment.model;

import lombok.Data;

/**
 * @ClassName Type
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/14
 * @Version V1.0
 **/
@Data
public class Type {
    private Integer id;
    private String name;
    private Integer parentId;
    private Integer level;
}
