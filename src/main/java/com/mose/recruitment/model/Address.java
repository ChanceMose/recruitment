package com.mose.recruitment.model;

import lombok.Data;

/**
 * @ClassName Address
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/11
 * @Version V1.0
 **/
@Data
public class Address {
    private Integer id;
    private String city;
    private String district;
    private String address;
    private Integer company_id;
}
