package com.mose.recruitment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName ApplyList
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/7
 * @Version V1.0
 **/
@Data
public class ApplyList {
    private Integer id;
    private Integer resume_id;
    private Integer hr_id;
    private String position;
    // 前端到后端
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    // 后端到前端，timezone = "GMT+8"转换时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date date;
    private Integer state;
    private String image;
    private String realName;
    private String sex;
    private String education;
    private String years;
    private String city;
    private String telephone;
    private String email;
}
