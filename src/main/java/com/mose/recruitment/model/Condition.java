package com.mose.recruitment.model;

import lombok.Data;

/**
 * @ClassName Condition
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/6
 * @Version V1.0
 **/
@Data
public class Condition {
    private Integer id;
    private String name;
    private Integer state;
}
