package com.mose.recruitment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Blob;
import java.util.Date;

/**
 * @ClassName Company
 * @Description: 公司实体类
 * @Author mose
 * @Date 2020/3/6
 * @Version V1.0
 **/
@Data
public class Company {
    private Integer id;
    private String shortName;
    private String fullName;
    private String type;
    private String image;
    private String word;
    private String financingType;
    private String population;
    private String homepage;
    private String labels;
    private String introduction;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createTime;
    private String capital;
    private String representative;
    private Integer legal;
}
