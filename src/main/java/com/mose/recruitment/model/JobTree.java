package com.mose.recruitment.model;

import com.mose.recruitment.service.TreeService;
import lombok.Data;

import java.util.List;

/**
 * @ClassName JobTree
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/14
 * @Version V1.0
 **/
@Data
public class JobTree implements TreeService<JobTree> {
    private Integer id;
    private String name;
    private Integer parentId;
    private Integer level;
    public List<JobTree> children;
}
