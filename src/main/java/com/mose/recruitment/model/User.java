package com.mose.recruitment.model;

import lombok.Data;

/**
 * @ClassName User
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/12
 * @Version V1.0
 **/
@Data
public class User {
    private Integer id;
    private String name;
    private String telephone;
    private String password;
    private String image;
    private Integer state;
}
