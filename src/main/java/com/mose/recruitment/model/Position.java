package com.mose.recruitment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName Position
 * @Description: 招聘信息实体
 * @Author mose
 * @Date 2020/2/28
 * @Version V1.0
 **/
@Data
public class Position {
    private Integer id;
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date create_time;
    private Integer salary_min;
    private Integer salary_max;
    private String city;
    private String district;
    private String address;
    private String years;
    private String education;
    private String advantage;
    private String description;
    private Integer hr_id;
    private Integer state;
    private Integer legal;
}
