package com.mose.recruitment.model;

import lombok.Data;

/**
 * @ClassName Role
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/12
 * @Version V1.0
 **/
@Data
public class Role {
    private Integer id;
    private String name;
    private String detail;
}
