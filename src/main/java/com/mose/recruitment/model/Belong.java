package com.mose.recruitment.model;

import lombok.Data;

/**
 * @ClassName Belong
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/28
 * @Version V1.0
 **/
@Data
public class Belong {
    private Integer id;
    private Integer company_id;
    private String realName;
    private String position;
    private String image;
    private String email;
}
