package com.mose.recruitment.model.resume;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName ResumeList
 * @Description: 简历列表实体类
 * @Author mose
 * @Date 2020/4/5
 * @Version V1.0
 **/
@Data
public class ResumeList {
    private Integer id;
    private String realName;
    private String image;
    private String sex;
    private String wanted;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date birth;
    private String education;
    private String city;
    private String years;
}
