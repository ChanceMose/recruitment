package com.mose.recruitment.model.resume;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName BaseInfo
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/30
 * @Version V1.0
 **/
@Data
public class BaseInfo {
    private Integer id;
    private String realName;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date birth;
    private String sex;
    private String city;
    private String email;
    private String education;
    private String years;
}
