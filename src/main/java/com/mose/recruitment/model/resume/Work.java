package com.mose.recruitment.model.resume;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName Work
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/2
 * @Version V1.0
 **/
@Data
public class Work {
    private Integer id;
    private String company;
    private String label;
    private String department;
    private String type;
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date entry;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date quit;
    private String description;
    private Integer user_id;
}
