package com.mose.recruitment.model.resume;

import lombok.Data;

/**
 * @ClassName Intention
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/2
 * @Version V1.0
 **/
@Data
public class Intention {
    private Integer id;
    private String wanted;
    private String city;
    private Integer salary_min;
    private Integer salary_max;
    private Integer user_id;
}
