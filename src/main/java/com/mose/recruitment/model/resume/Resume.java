package com.mose.recruitment.model.resume;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Blob;
import java.util.Date;

/**
 * @ClassName Resume
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/12
 * @Version V1.0
 **/
@Data
public class Resume {
    private Integer id;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date create_time;
    private String image;
    private String address;
    private String state;
    private String apply;
    private String description;
    private String file;
    private Integer visible;
}
