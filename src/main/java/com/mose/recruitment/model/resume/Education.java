package com.mose.recruitment.model.resume;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName Education
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/1
 * @Version V1.0
 **/
@Data
public class Education {
    private Integer id;
    private String school;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date entrance;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date graduation;
    private String record;
    private String speciality;
    private Integer state;
    private Integer user_id;
}
