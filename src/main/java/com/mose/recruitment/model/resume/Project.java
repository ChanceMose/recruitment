package com.mose.recruitment.model.resume;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName Project
 * @Description: TODO
 * @Author mose
 * @Date 2020/4/2
 * @Version V1.0
 **/
@Data
public class Project {
    private Integer id;
    private String name;
    private String company;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date start;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date end;
    private String url;
    private String description;
    private String achievement;
    private Integer user_id;

}
