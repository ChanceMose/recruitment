package com.mose.recruitment.utils;
import java.util.Base64;

/**
 * @ClassName Base64Util
 * @Description: base64进行加密解密
 * @Author mose
 * @Date 2020/2/27
 * @Version V1.0
 **/
public class Base64Util {

    /**
     * @param ciphertext
     * @MethodName: decode
     * @Description: 文字解密
     * @Return: java.lang.String
     * @Author: mose
     * @Date: 2020/2/27 15:37
     */
    public static String decode(String ciphertext) {
        String plaintext = null;
        try {
            plaintext = new String(Base64.getDecoder().decode(ciphertext), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return plaintext;
    }

    /**
     * @param plaintext
     * @MethodName: encode
     * @Description: 文字加密
     * @Return: java.lang.String
     * @Author: mose
     * @Date: 2020/2/27 15:38
     */
    public static String encode(String plaintext) {
        String ciphertext = null;
        try {
            ciphertext = Base64.getEncoder().encodeToString(plaintext.getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ciphertext;
    }
}
