package com.mose.recruitment.utils;

import com.mose.recruitment.service.TreeService;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName TreeParser
 * @Description: 解析树型数据工具类
 * @Author mose
 * @Date 2020/2/23
 * @Version V1.0
 **/
public class TreeParser {

    /**
     * @param topId
     * @param entityList
     * @MethodName: getTreeList
     * @Description: 解析树形数据
     * @Return: java.util.List<E>
     * @Author: mose
     * @Date: 2020/2/23 19:59
     */
    public static <E extends TreeService<E>> List<E> getTreeList(Integer topId, List<E> entityList) {
        List<E> resultList = new ArrayList<>();

//        获取顶层元素合集
        Integer parentId;
        for (E entity : entityList) {
            parentId = entity.getParentId();
            if (parentId == null || topId == parentId) {
                resultList.add(entity);
            }
        }

//        获取每个顶层元素的子数据集合
        for (E entity : resultList) {
            entity.setChildren(getSubList(entity.getId(), entityList));
        }

        return resultList;
    }

    /**
     * @param id
     * @param entityList
     * @MethodName: getSubList
     * @Description: 获取子数据集合
     * @Return: java.util.List<E>
     * @Author: mose
     * @Date: 2020/2/23 20:06
     */
    private static <E extends TreeService<E>> List<E> getSubList(Integer id, List<E> entityList) {
        List<E> childList = new ArrayList<>();
        Integer parentId;

//        子集的直接子对象
        for (E entity : entityList) {
            parentId = entity.getParentId();
            if (id == parentId) {
                childList.add(entity);
            }
        }

//        子集的间接子对象
        for (E entity : childList) {
            entity.setChildren(getSubList(entity.getId(), entityList));
        }

//        递归退出条件
        if (childList.size() == 0) {
            return null;
        }

        return childList;
    }
}
