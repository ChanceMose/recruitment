package com.mose.recruitment.utils;

import com.mose.recruitment.config.StatusCode;
import com.mose.recruitment.model.send.MyException;
import com.mose.recruitment.model.send.MyResult;

/**
 * @ClassName ResultUtil
 * @Description: TODO
 * @Author mose
 * @Date 2020/3/17
 * @Version V1.0
 **/
public class ResultUtil {
    public static MyResult success(Object object) {
        MyResult result = new MyResult();
        result.setCode(StatusCode.SUCCESS.getCode());
        result.setMsg(StatusCode.SUCCESS.getMsg());
        result.setData(object);
        return result;
    }

    public static MyResult success() {
        return success(null);
    }

    public static MyResult noExit(String msg) {
        MyResult result = new MyResult();
        result.setCode(StatusCode.NOT_EXIT.getCode());
        result.setMsg(msg);
        return result;
    }

    public static MyResult noExit() {
        return noExit(StatusCode.NOT_EXIT.getMsg());
    }

    public static MyResult successUpdate(String msg) {
        MyResult result = new MyResult();
        result.setCode(StatusCode.SUCCESS_UPDATE.getCode());
        result.setMsg(msg);
        return result;
    }

    public static MyResult failUpdate(String msg) {
        MyResult result = new MyResult();
        result.setCode(StatusCode.ERROR.getCode());
        result.setMsg(msg);
        return result;
    }

    public static MyResult successDel() {
        MyResult result = new MyResult();
        result.setCode(StatusCode.SUCCESS_DEL.getCode());
        result.setMsg(StatusCode.SUCCESS_DEL.getMsg());
        return result;
    }

    public static MyResult failDel() {
        MyResult result = new MyResult();
        result.setCode(StatusCode.ERROR.getCode());
        result.setMsg("删除失败");
        return result;
    }

    public static MyResult error(Integer code, String msg) {
        MyResult result = new MyResult();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public static MyResult error(StatusCode statusCode) {
        return error(statusCode.getCode(), statusCode.getMsg());
    }

    public static MyResult error(MyException myException) {
        return error(myException.getCode(), myException.getMessage());
    }
}
