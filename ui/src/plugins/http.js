/**
 * axios请求配置
 */
import axios from 'axios'
import store from '../store'
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/'

// 请求拦截
axios.interceptors.request.use(
  config => {
    // 判断token存在   登录拦截
    if (localStorage.token) {
      // 设置统一的header
      config.headers.token = localStorage.token
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 响应拦截
axios.interceptors.response.use(
  response => {
    if (response.data.code === 401) {
      localStorage.removeItem('token')
      if (localStorage.getItem('role') === '1') {
        store.dispatch('toggleVisible', true)
      } else {
        window.location.href = '/login'
      }
    }
    return response
  },
  error => {
    return Promise.reject(error)
  }
)

export default axios
