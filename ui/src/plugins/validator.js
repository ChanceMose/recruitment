/* 是否手机号码 */
export function validatePhone(rule, value, callback) {
  const reg = /^[1][3,4,5,7,8][0-9]{9}$/
  if (value === '' || value === undefined || value === null) {
    callback()
  } else {
    if (!reg.test(value) && value !== '') {
      callback(new Error('请输入有效的电话号码'))
    } else {
      callback()
    }
  }
}

/* 是否邮箱 */
export function validateEMail(rule, value, callback) {
  const reg = /^([a-zA-Z0-9]+[_|-|.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|-|.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,4}$/
  if (value === '' || value === undefined || value === null) {
    callback()
  } else {
    if (!reg.test(value)) {
      callback(new Error('请输入有效的邮箱地址'))
    } else {
      callback()
    }
  }
}

/* 是否邮箱或者手机号码 */
export function validatePhoneOrEMail(rule, value, callback) {
  const reg = /^[1][3,4,5,7,8][0-9]{9}$/
  const mailReg = /^([a-zA-Z0-9]+[_|-|.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|-|.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,4}$/

  if (value.indexOf('@') > 0) {
    // 含有字母@
    setTimeout(() => {
      if (mailReg.test(value)) {
        callback()
      } else {
        callback(new Error('请输入正确的邮箱'))
      }
    }, 100)
  } else {
    const re = new RegExp(reg)
    if (!re.test(value)) {
      callback(new Error('请输入正确的电话'))
    } else {
      callback()
    }
  }
}
/* 是否6位数字 */
export function validateSixNumber(rule, value, callback) {
  const reg = /^\d{6}$/

  if (value === '' || value === undefined || value === null) {
    callback()
  } else {
    if (!reg.test(value) && value !== '') {
      callback(new Error('请输入6位数字验证码'))
    } else {
      callback()
    }
  }
}
