import Vue from 'vue'
Vue.filter('timeFilter', function(value) {
  // 创建日期对象
  var data = new Date(value)
  var currentDate = new Date()

  // 获取月日时分
  var y = data.getFullYear()
  var m = data.getMonth() + 1
  var d = data.getDate()
  var h = data.getHours()
  var M = data.getMinutes()

  var yc = currentDate.getFullYear()
  var mc = currentDate.getMonth() + 1
  var dc = currentDate.getDate()

  if (m === mc && d === dc) {
    h < 10 && (h = '0' + h)
    M < 10 && (M = '0' + M)
    return `${h}:${M} `
  } else if (m === mc && dc - d < 3 && dc - d > 0) {
    return `${dc - d} 天前`
  } else {
    // 修改月日格式
    m < 10 && (m = '0' + m)
    d < 10 && (d = '0' + d)
    if (y !== yc) {
      return `${y}-${m}-${d} `
    }
    return `${m}-${d} `
  }
})

Vue.filter('ageFilter', function(value) {
  // 创建日期对象
  var data = new Date(value)
  var currentDate = new Date()
  if (value === '') {
    return '年龄'
  }
  // 获取年
  var y = data.getFullYear()
  // console.log(y)

  var yc = currentDate.getFullYear()
  // console.log(yc)
  return yc - y + '岁'
})
