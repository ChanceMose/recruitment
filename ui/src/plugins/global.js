const baseUrl = '//localhost:8888/images/'
const images = {
  base: baseUrl,
  company: baseUrl + 'default_company.png',
  hr: baseUrl + 'default_hr.png',
  product: baseUrl + 'default_product.png',
  user: baseUrl + 'default_user.png',
  ad: baseUrl + 'default_ad.png',
  resume: baseUrl + 'default_resume.png',
  admin: baseUrl + 'default_admin.gif'
}

export default {
  images
}
