import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    // 面板显示
    visible: false,
    // 角色
    role: 1
  },
  mutations: {
    toggleVisible(state, status) {
      state.visible = status
    },
    changeRole(state, status) {
      state.role = status
    }
  },
  actions: {
    toggleVisible({ commit }, status) {
      commit('toggleVisible', status)
    },
    changeRole({ commit }, status) {
      commit('changeRole', status)
    }
  },
  modules: {}
})

export default store
