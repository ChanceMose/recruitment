import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'

import HomePage from '../views/reception/HomePage.vue'
import PositionDetail from '../views/reception/PositionDetail.vue'
import CompanyDetail from '../views/reception/CompanyDetail.vue'
import PositionSearch from '../views/reception/PositionSearch.vue'
import CompanySearch from '../views/reception/CompanySearch.vue'
import Message from '../views/reception/Message.vue'
import Register from '../views/reception/Register.vue'
import CompanyHome from '../views/reception/CompanyHome.vue'
import Edit from '../views/reception/EditCompany.vue'
import DropBox from '../views/reception/UserDropBox.vue'
import Collection from '../views/reception/Collection.vue'
import Resume from '../views/reception/Resume.vue'
import Preview from '../views/reception/Preview.vue'
import Setting from '../views/reception/Setting.vue'
import Login from '../views/reception/Login.vue'
import Open from '../views/reception/CompanyOpen.vue'
import HrSetting from '../views/reception/HrSetting.vue'
import Init from '../views/reception/UserInit.vue'

import AdminLogin from '../views/backstage/Login.vue'
import Frame from '../views/backstage/Frame.vue'
import AdminHome from '../components/backstage/Home.vue'
import AdminPerson from '../components/backstage/Person.vue'
import User from '../components/backstage/User.vue'
import Type from '../components/backstage/Type.vue'
import Company from '../components/backstage/Company.vue'
import Position from '../components/backstage/Position.vue'
import Menu from '../components/backstage/Menu.vue'

Vue.use(VueRouter)

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const routes = [
  { path: '/', redirect: '/home' },
  { path: '/home', component: HomePage },
  { path: '/search/positions', component: PositionSearch },
  { path: '/search/company', component: CompanySearch },
  { path: '/positions/:id', name: 'positionDetail', component: PositionDetail },
  { path: '/company/home', component: CompanyHome },
  { path: '/company/open', component: Open },
  { path: '/company/:id', name: 'companyDetail', component: CompanyDetail },
  { path: '/hr/setting', component: HrSetting },
  { path: '/message', component: Message },
  { path: '/register', component: Register },
  { path: '/edit', component: Edit },
  { path: '/dropbox', component: DropBox },
  { path: '/collection', component: Collection },
  { path: '/resume', component: Resume },
  { path: '/preview/:id', component: Preview },
  { path: '/setting', component: Setting },
  { path: '/login', component: Login },
  { path: '/init', component: Init },

  {
    path: '/admin',
    redirect: '/admin/login'
  },
  { path: '/admin/login', component: AdminLogin },
  {
    path: '/back',
    component: Frame,
    redirect: 'home',
    children: [
      { path: 'home', component: AdminHome },
      { path: 'user', component: User },
      { path: 'type', component: Type },
      { path: 'company', component: Company },
      { path: 'position', component: Position },
      { path: 'menu', component: Menu },
      { path: 'person', component: AdminPerson }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// 路由守卫
router.beforeEach((to, from, next) => {
  // 判断 localstorage 里面是否有token
  const isLogin = localStorage.getItem('token')
  if (
    to.path === '/home' ||
    to.path === '/search/positions' ||
    to.path === '/search/company' ||
    to.name === 'companyDetail' ||
    to.name === 'positionDetail' ||
    to.path === '/message' ||
    to.path === '/register' ||
    to.path === '/talk' ||
    to.path === '/login' ||
    to.path === '/company/open' ||
    to.path === '/admin/login'
  ) {
    next()
  } else if (to.path === '/company/home') {
    // 如果为真，正常跳转，否则跳到登录页面
    if (isLogin) {
      if (localStorage.getItem('role') === '2') {
        next()
      } else {
        next({ path: '/company/open' })
      }
    } else {
      next({ path: '/login' })
    }
  } else if (to.path === '/back/home' || to.path === '/back/person') {
    // 如果为真，正常跳转，否则跳到登录页面
    if (isLogin === 'admin') {
      next()
    } else {
      localStorage.removeItem('token')
      localStorage.setItem('toPath', to.path)
      next({ path: '/admin/login' })
    }
  } else {
    // 如果为真，正常跳转，否则跳到登录页面
    if (isLogin) {
      next()
    } else {
      next(from.path)
      localStorage.removeItem('token')
      localStorage.setItem('toPath', to.path)
      store.dispatch('toggleVisible', true)
    }
  }
})

export default router
