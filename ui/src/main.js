import Vue from 'vue'
import App from './App.vue'
import VCharts from 'v-charts'
import router from './router'
import store from './store'
import axios from './plugins/http'
import './plugins/element.js'
// 导入全局样式表
import './assets/css/global.css'
// 导入第三方图标库
import './assets/css/iconfont.css'
// 导入时间过滤函数
import './plugins/date'

Vue.prototype.$http = axios
Vue.config.productionTip = false

Vue.use(VCharts)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
